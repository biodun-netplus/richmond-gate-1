<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_model');
        if (!$this->aauth->is_loggedin())
            redirect('login', 'refresh');
        if (!$this->aauth->is_member('Admin') && !$this->aauth->is_member('Merchant') && !$this->aauth->is_member('Public'))
            show_error('Access Denied');
    }

    public function all($userId = Null)
    {

        if ($this->aauth->is_member('Admin')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');
            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('product.product_type', 'Power');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->order_by("payments.date_created", "desc")->get('payments');
        }
        if ($this->aauth->is_member('Merchant')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');
            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <', $dateTo);

            }
            $this->db->where('product.product_type', 'Power');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');

            $query = $this->db->where('user_id', $this->aauth->get_user()->id)->order_by("payments.date_created", "desc")->get('payments');


        }

        if ($this->aauth->is_member('Public')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');
            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('product.product_type', 'Power');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->where('payments.user_id', $this->aauth->get_user()->id)->order_by("payments.date_created", "desc")->get('payments');

        }

        if ($userId != NULL) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');
            $this->db->where('product.product_type', 'Power');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $this->db->where('payments.type !=', 'Service');
            $query = $this->db->where('cart.user_id', $userId)->get('payments');
        }

        $recent_payments = $query->result();

        $data = array(
            'title' => 'My Payments',
            'payments' => $recent_payments,
            'date_from' => $this->input->post('date_from'),
            'date_to' => $this->input->post('date_to')
        );
        $this->template->load('default', 'account/payments', $data);
    }

    public function pay()
    {
        if ($this->aauth->is_member('Admin'))
            redirect('payment/list', 'refresh');
        $data = array(
            'title' => 'New Payment',
        );
        if ($this->input->post()) {
            $this->form_validation->set_rules('amount', 'Amount', 'required|numeric');
            $this->form_validation->set_rules('meter_no', 'Meter No', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            if ($this->form_validation->run() == TRUE) {
                $amount = $this->input->post('amount');
                $description = $this->input->post('description');
                $meter_no = $this->input->post('meter_no');
                $paymentid = 'EPAY' . time();

                $data = array(
                    'payment_id' => $paymentid,
                    'user_id' => $this->aauth->get_user()->id,
                    'meter_no' => $meter_no,
                    'narration' => $description,
                    'amount' => $amount,
                    'status' => 'Pending',
                );

                $this->db->insert('payments', $data);

                $data = array(
                    'amount' => $amount,
                    'narration' => $description,
                    'order_id' => $paymentid,
                );
                return $this->load->view('pay', $data);

            }
            $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors')));

            $this->session->set_flashdata('errors', $errors);

            return redirect('payment/pay');
        }

        $this->template->load('default', 'account/pay', $data);
    }


    public function callback()
    {
        $payment_id = $_POST['order_id'];

        $statusCode = $_POST['code'];
        $trans_ref = $_POST['transaction_id'];
        $reason = $_POST['description'];
        $amount_paid = $_POST['amount_paid'];
        $bank = $_POST['bank'];

        if (!empty($payment_id) & !empty($trans_ref)) {

            if ($payment = $this->payment_model->get(array('payment_id' => $payment_id))) {

                switch ($statusCode) {
                    case "00":
                        $payment_status = "Paid";
                        break;
                    default:
                        $payment_status = "Payment Failed";

                }


                $data = array(
                    'transaction_id' => $trans_ref,
                    'bank' => $bank,
                    'amount_paid' => $amount_paid,
                    'payment_description' => $reason,
                    'status' => $payment_status,
                );


                $this->payment_model->update($data, $payment->id);

                if ($statusCode === "00") {
                    $this->session->set_flashdata('success', 'Payment Completed Successfully.');
                    //exit;
                    return redirect('payment/list');

                }

                if ($statusCode != "00") {

                    $error = array('reason' => $reason, 'code' => $statusCode, 'payment_id' => '');

                }

            } else {
                $error = array('reason' => 'Payment ID invalid', 'code' => $statusCode, 'payment_id' => '');
            }

        } else {
            $error = array('reason' => 'Missing parameters. ' . $reason, 'code' => '500', 'payment_id' => '');
        }


        $this->session->set_flashdata('payment_failed', $error);
        return redirect('payment/list');


    }


    public function success()
    {
        if ($data = $this->session->flashdata('payment_success')) {
            return $this->load->view('pay-success', $data);
        }
        return $this->load->view('errors/custom-not');
    }

    public function failed()
    {

        if ($data = $this->session->flashdata('payment_failed')) {
            return $this->load->view('pay-failed', $data);
        }
        return $this->load->view('errors/custom-not');

    }

    function outstanding_list()
    {
        if ($this->aauth->is_member('Admin')) {
            $this->db->select('*,payments.status as p_status,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('outstanding_bills', 'outstanding_bills.id = cart.product_id');
            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('cart.product_type', 'BILL');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->order_by("payments.date_created", "desc")->get('payments');
        }
        if ($this->aauth->is_member('Public')) {
            $this->db->select('*,payments.status as p_status,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('outstanding_bills', 'outstanding_bills.id = cart.product_id');
            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('cart.product_type', 'BILL');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->where('payments.user_id', $this->aauth->get_user()->id)->order_by("payments.date_created", "desc")->get('payments');
        }
        $recent_payments = $query->result();
        $data = array(
            'title' => 'My Outstanding Payments',
            'payments' => $recent_payments,
            'date_from' => $this->input->post('date_from'),
            'date_to' => $this->input->post('date_to')
        );
        $this->template->load('default', 'shopping/outstanding_payments', $data);
    }

}