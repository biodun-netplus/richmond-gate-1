<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shopping extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('Product_model');
        $this->load->model('Payment_model');
        $this->load->model('Coupon_model');
        $this->load->model('Outstanding_model');
        $this->load->library('cart');
        if (!$this->aauth->is_loggedin())
            redirect('login', 'refresh');

        if (!$this->aauth->is_member('Admin') && !$this->aauth->is_member('Merchant') && !$this->aauth->is_member('Public'))
            show_error('Access Denied');
    }

    public function index()
    {

        $data['indFirSec'] = $this->Front_model->select_using_single_condition('home_content', 'id', '27');
        $data['products'] = $this->billing_model->get_all();
        $this->load->view('shopping_view', $data);
    }


    function add()
    {
        $_SESSION['cart_session'] = uniqid() . session_id();
        //$this->db->query("DELETE FROM cart WHERE cart_session='".$_SESSION['cart_session']."'");
        $_SESSION['order_id'] = null;
        $c_user = $this->aauth->get_user();
        $userId = $c_user->id;
        $meter_no = $c_user->meter_no;
        $productId = $this->input->post('id');
        $productname = $this->input->post('name');
        $productprice = $this->input->post('price');   
        $bill_payment = $this->input->post('bill_payment');
        $product_type = (isset($bill_payment) && $bill_payment == 'true') ? 'BILL' : 'PRODUCT';
        $product = $this->db->query("select * from cart where user_id='" . $userId . "' AND product_id='" . $productId . "' AND cart_session='" . $_SESSION['cart_session'] . "' AND order_id IS NULL")->result();
        //echo count($product);die;
        $scp = $this->db->query("select * from `product` inner join `aauth_users` on `product`.`property_type` = `aauth_users`.`type_of_property` where meter_no='".$meter_no."' and product_type='Service'")->row();
        $productPrice = $scp->product_price;
        $actualServiceFee = $this->input->post('price');
    
        if($actualServiceFee >=  $productPrice){
            $quantity = $actualServiceFee / $productPrice;
        }else{
            $quantity = 1;
        }
        if (count($product) <= 0) {
            // Set array for send data.
            $insert_data = array(
                'user_id' => $userId,
                'payment_status' => 'Pending',
                'product_id' => $this->input->post('id'),
                'product_name' => $this->input->post('name'),
                'price' => $this->input->post('price'),
                'cart_session' => $_SESSION['cart_session'],
                'meter_no' => $meter_no,
                'product_type' => $product_type,
                'quantity' => $quantity,
                'date' => date('Y-m-d H:i:s')
            );

            $stts = $this->Product_model->setRecord('cart', $insert_data);
        }

        redirect('shopping/cart');
    }

    function cart($action = NULL, $CartId = NULL)
    {
        
        $data = array(
            'title' => 'Cart Details'
        );
        $data['c_user'] = $this->aauth->get_user();
        $userId = $data['c_user']->id;
        $meter_no = $data['c_user']->meter_no;

        if (!empty($action) && $action == 'delete') {

            if ($product = $this->db->query("DELETE FROM cart WHERE cart_id='" . $CartId . "'")) {
                return redirect('shopping/cart');

            }
        }
       

        if (!empty($_SESSION['cart_session'])) {

            $cart = $this->db->select('*')->from('cart')->where(array('cart.user_id' => $userId, 'cart.cart_session' => $_SESSION['cart_session']))->get()->row();
            if(!$cart){
                return redirect('product/product');
            }

   
             $product = $this->db->select('*')->from('product')->where(array('product.id' => $cart->product_id))->get()->row();


            if($cart->product_type == 'PRODUCT' && $product->product_type == 'Service' ){
                $plan = $this->db->query("SELECT * FROM service_charges WHERE meter_no = '$meter_no'")->row();
                if(!is_null($plan->payment_type_id)){
                    $this->db->select('service_charges.id, service_charges.amount_due, product.id,product.product_type,product.product_name,product.product_price,product.base_charge,product.take_or_pay,cart.quantity,cart.price,cart.cart_id,cart.product_price_type,payment_type.occurrence');
                    $this->db->from('cart');
                    $this->db->join('product', 'cart.product_id=product.id');
                    $this->db->join('service_charges', 'service_charges.product_id=product.id');
                    $this->db->join('payment_type', 'payment_type.id = service_charges.payment_type_id');
                    $this->db->where(array('cart.meter_no' => $meter_no, 'service_charges.meter_no' => $meter_no, 'cart.cart_session' => $_SESSION['cart_session']));
                    $query = $this->db->get();
                    $data['records'] = $query->result();
                }else{
                    $this->db->select('service_charges.id, service_charges.amount_due, product.id,product.product_type,product.product_name,product.product_price,product.base_charge,product.take_or_pay,cart.quantity,cart.price,cart.cart_id,cart.product_price_type,payment_type.occurrence');
                    $this->db->from('cart');
                    $this->db->join('product', 'cart.product_id=product.id');
                    $this->db->join('payment_type', 'product.payment_type_id = payment_type.id');
                    $this->db->join('service_charges', 'service_charges.product_id=product.id');
                    $this->db->where(array('cart.meter_no' => $meter_no, 'service_charges.meter_no' => $meter_no, 'cart.cart_session' => $_SESSION['cart_session']));
                    $query = $this->db->get();
                    $data['records'] = $query->result();
                }

                $this->template->load('default', 'shopping/product_cart', $data);
               
            }else if($cart->product_type == 'PRODUCT'){
                $this->db->select('product.id,product.product_type,product.product_name,product.product_price,product.base_charge,product.take_or_pay,cart.quantity,cart.price,cart.cart_id,cart.product_price_type');
                $this->db->from('cart');
                $this->db->join('product', 'cart.product_id=product.id');
                $this->db->where(array('cart.user_id' => $userId, 'cart.cart_session' => $_SESSION['cart_session']));
                $query = $this->db->get();
                $data['records'] = $query->result();
          
                $this->template->load('default', 'shopping/product_cart', $data);
            }else if($cart->product_type == 'BILL' && $product->product_type == 'Power'){
                $this->db->select('product.id,product.product_type,product.product_name,product.product_price,product.base_charge,product.take_or_pay,cart.quantity,cart.price,cart.cart_id,cart.product_price_type');
                $this->db->from('cart');
                $this->db->join('product', 'cart.product_id=product.id');
                $this->db->where(array('cart.user_id' => $userId, 'cart.cart_session' => $_SESSION['cart_session']));
                $query = $this->db->get();
                $data['records'] = $query->result();

                $this->template->load('default', 'shopping/product_cart', $data);
            }else if($cart->product_type == 'BILL'){
                $this->db->select('outstanding_bills.*,cart.quantity,cart.price,cart.cart_id,cart.product_price_type,cart.product_type');
                $this->db->from('cart');
                $this->db->join('outstanding_bills', 'cart.product_id=outstanding_bills.id');
                $this->db->where(array('cart.user_id' => $userId, 'cart.cart_session' => $_SESSION['cart_session']));
                $query = $this->db->get();
                $data['records'] = $query->result();
                $this->template->load('default', 'shopping/bill_cart', $data);
            }

        } else {
            return redirect('product/product');

        }

    }

    public function couponpay()
    {
        if (isset($_POST['pay_with_coupon'])) {
            if (!empty($_SESSION['cart_session'])) {
                $c_user = $this->aauth->get_user();
                if (empty($c_user->meter_no)) {
                    $this->session->set_flashdata('flashMsg', 'Sorry meter number cannot be empty, plese update your meter number.');
                    return redirect('shopping/cart');
                }
                $this->db->select('product.id,product.product_type,product.product_name,product.product_price,product.base_charge,product.take_or_pay,cart.quantity,cart.price,cart.cart_id,cart.product_price_type');
                $this->db->from('cart');
                $this->db->join('product', 'cart.product_id=product.id');
                $this->db->where(array('cart.user_id' => $c_user->id, 'cart.cart_session' => $_SESSION['cart_session']));
                $query = $this->db->get();
                $cart = $query->row();
               
                $coupon_verify = $this->Coupon_model->verify(['code' => $this->input->post('coupon_code'), 'user_id' => $c_user->id, 'product_id' => $cart->id]);
                if ($coupon_verify) {
                    if ($coupon_verify->total_usage >= $coupon_verify->usage_count) {
                        //Should check if coupon is equal to Service Charge.
                        if ($coupon_verify->coupon_value >= $cart->product_price) {
                            $coupon_status = ($coupon_verify->total_usage == ($coupon_verify->usage_count + 1)) ? 1 : 0;
                            if (!isset($_SESSION['order_id'])) {
                                $_SESSION['order_id'] = "55" . $this->GenerateOrder(10);
                            }

                            $order_id = $_SESSION['order_id'];

                            $Maildata = array();
                            $Maildata['full_name'] = $c_user->full_name;
                            $Maildata['to'] = $c_user->email;
                            $Maildata['order_id'] = $order_id;
                            $Maildata['trans_ref'] = $coupon_verify->code;
                            $Maildata['bank'] = '';
                            $Maildata['amount_paid'] = $coupon_verify->coupon_value;
                            $Maildata['reason'] = '';
                            $Maildata['payment_status'] = 'Pending';
                            $Maildata['tr_date'] = date('d/m/YYYY');
                            $Maildata['tr_time'] = date('H:i');
                            $Maildata['token_no'] = '';
                            $Maildata['token_desc'] = '';
                            $Maildata['token_amount'] = '';
                            $Maildata['is_power'] = 0;
                            $Maildata['product_name'] = '';
                           
                            $product_price = $cart->product_price;
                            $this->db->query("update cart set price='" . $product_price . "', quantity='1', product_price_type='Total',payment_status = 'Pending', meter_no='" . $c_user->meter_no . "', order_id = '" . $order_id . "' where cart_session='" . $_SESSION['cart_session'] . "' and user_id='" . $c_user->id . "' and  product_id='" . $cart->id . "'");
                            $this->db->query("INSERT INTO payments set status = 'Pending', meter_no='" . $c_user->meter_no . "' , payment_id = '" . $order_id . "' , amount ='" . $product_price . "',user_id='" . $c_user->id . "' , date_created='" . date('Y-m-d H:i:s') . "',type='".$cart->product_type."'");


                            $payment = $this->Product_model->get(array('payment_id' => $order_id));
                            $updatedata = ['payment_type' => 'Coupon', 'coupon_id' => $coupon_verify->id];
                            
                            if ($cart->product_type == 'Power' || $cart->product_type == 'Top Up') {
                               
                                if($cart->product_type === 'Power'){
                                   
                                    $Maildata['is_power'] = 1;
                                    //Post Data
                                    $Tokenamount = $coupon_verify->coupon_value;
                        
                                    $powerToken = $this->generatePowerToken($Tokenamount, $order_id);
                                    //echo "Vendin api call after send token".'<br>';
                                    if ($powerToken) {

                                        $Maildata['token_desc'] = $powerToken->responsedesc;
                                        $Maildata['token_amount'] = $powerToken->unit;
                                        if ($powerToken->responsecode == 00) {
                                            $Maildata['payment_status'] = $updatedata['status'];
                                            $Maildata['token_no'] = $powerToken->recieptNumber;
                                            $updatedata['status'] = 'Paid';
                                            $updatedata['token_no'] = $powerToken->recieptNumber;
                                            $updatedata['token_desc'] = $powerToken->responsedesc;
                                            $updatedata['token_amount'] = $powerToken->unit;
                                            $this->Product_model->update(['vend_log'=>json_encode($updatedata)], $payment_verify->id);
                                           // $this->Product_model->update($updatedata, $payment->id);
                                            $this->Coupon_model->update(['status' => $coupon_status,'usage_count'=>($coupon_verify->usage_count + 1), 'order_id' => $order_id], $coupon_verify->id);
                                            $this->successpay($Maildata, $order_id);
                                        } else {
                                            $updatedata['status'] = 'Payment Failed';
                                            $Maildata['payment_status'] = $updatedata['status'];
                                            $updatedata['token_desc'] = $powerToken->responsedesc;
                                            $updatedata['token_amount'] = $powerToken->unit;
                                            $this->Product_model->update($updatedata, $payment->id);
                                            $this->failedpayment($Maildata, $order_id);
                                        }
                                    } else {
                                        $this->session->set_flashdata('errors', 'Unable to generate token. please try again');
                                    }
                                }else{
                                    $Maildata['is_power'] = 1;
                                    //Post Data
                                    $Tokenamount = $coupon_verify->coupon_value;
                                    $powerToken = $this->generatePowerToken($Tokenamount, $order_id);
                                    //echo "Vendin api call after send token".'<br>';
                                    if ($powerToken) {

                                        $Maildata['token_desc'] = $powerToken->responsedesc;
                                        $Maildata['token_amount'] = $powerToken->unit;
                                        if ($powerToken->responsecode == 00) {
                                            $updatedata['status'] = 'Paid';
                                            $Maildata['payment_status'] = $updatedata['status'];
                                            $Maildata['token_no'] = $powerToken->recieptNumber;
                                            $updatedata['token_no'] = $powerToken->recieptNumber;
                                            $updatedata['token_desc'] = $powerToken->responsedesc;
                                            $updatedata['token_amount'] = $powerToken->unit;
                                            $this->Product_model->update($updatedata, $payment->id);
                                            $this->Coupon_model->update(['status' => $coupon_status,'usage_count'=>($coupon_verify->usage_count + 1), 'order_id' => $order_id], $coupon_verify->id);
                                            $this->successpay($Maildata, $order_id);
                                        } else {
                                            $updatedata['status'] = 'Payment Failed';
                                            $Maildata['payment_status'] = $updatedata['status'];
                                            $updatedata['token_desc'] = $powerToken->responsedesc;
                                            $updatedata['token_amount'] = $powerToken->unit;
                                            $this->Product_model->update($updatedata, $payment->id);
                                            $this->failedpayment($Maildata, $order_id);
                                        }
                                    } else {
                                        $this->session->set_flashdata('errors', 'Unable to generate token. please try again');
                                    }
                                }
                                
                                $this->Product_model->update($updatedata, $payment->id);
                                return redirect('shopping/cart');
                            } else {
                                $product_price = $coupon_verify->coupon_value;
                                
                                $updatedata['status'] = 'Paid';

                                //get service charge by meter no
                                

                                if(isset($c_user->meter_no)){
                                    $MeterNo = $c_user->meter_no;
                                    $detail = $this->Product_model->getDetails($MeterNo);
                                    $amount_due = (int)$detail[0]->amount_due;
                                    $totalpaid = (int)$detail[0]->total_paid;
                                    $advance_payment = 0;
                                    $advance_month = 0;
                                    
                                    $amount = $amount_due - $product_price;
                                    $totalpaid = $totalpaid + $product_price;
                                    $this->Product_model->updateServiceCharge($MeterNo, $amount, $totalpaid, $advance_payment, $advance_month);
                                }else{
                                    $this->session->set_flashdata('errors', 'Meter No does not exist.');
                                }
                                   
          
                                

                                $this->Product_model->update($updatedata, $payment->id);
                                $this->Coupon_model->update(['status' => $coupon_status,'usage_count'=>($coupon_verify->usage_count + 1), 'order_id' => $order_id], $coupon_verify->id);
                                $this->successpay($Maildata, $order_id);
                            }
                        } else {
                            $this->session->set_flashdata('errors', 'Coupon code amount (' . number_format($coupon_verify->coupon_value) . ') cannot be used for product.');
                        }
                    }else {
                        $this->session->set_flashdata('errors', 'Invalid coupon code - Coupon already exhausted.');
                    }
                } else {
                    $this->session->set_flashdata('errors', 'Invalid coupon code.');
                }
            } else {
                $this->session->set_flashdata('errors', 'No product added to cart.');
            }
        }
        return redirect('shopping/cart');
    }

    private function successpay($Maildata, $payment_id)
    {
        $to = $Maildata['to'];
        $subject = 'Payment Completed successful';
        $config = array(
            'mailtype' => 'html',
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_user' => 'saddle@netplusadvisory.com',
            'smtp_pass' => 'Saddle7890',
            'smtp_port' => 465,
            'priority' => 1,
            'newline' => "\r\n"
        );

        $message = $this->load->view('shopping/pay-success.php', $Maildata, true);

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('saddle@netplusadvisory.com', 'Richmond Gate Phase 1 Estate Association');
        $this->email->to($to);

        $this->email->subject($subject);
        $this->email->message($message);

        $r = $this->email->send();

        unset($_SESSION['order_id']);
        unset($_SESSION['cart_session']);

        redirect('shopping/pay_success/' . $payment_id, $Maildata);
    }

    private function failedpayment($Maildata, $payment_id)
    {
        $to = $Maildata['to'];
        $config = array(
            'mailtype' => 'html',
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_user' => 'saddle@netplusadvisory.com',
            'smtp_pass' => 'Saddle7890',
            'smtp_port' => 465,
            'priority' => 1,
            'newline' => "\r\n"
        );

        $message = $this->load->view('shopping/pay-failed.php', $Maildata, true);

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('saddle@netplusadvisory.com', 'Richmond Gate Phase 1 Estate Association');
        $this->email->to($to);

        $this->email->subject('Payment Failed');
        $this->email->message($message);

        $r = $this->email->send();

        unset($_SESSION['order_id']);
        unset($_SESSION['cart_session']);

        redirect('shopping/pay_failed/' . $payment_id, $Maildata);
    }


    function checkout()
    {
        if (!empty($_SESSION['cart_session'])) {
            $data['c_user'] = $this->aauth->get_user();
           
            $check_out = $this->input->post('check_out');
            $merNo = $data['c_user']->meter_no;
    
            $data = array(
                'title' => 'Check Out '
            );



            if (!isset($_SESSION['order_id'])) {
                $_SESSION['order_id'] = "55" . $this->GenerateOrder(10);
            }

            if ($this->input->post('product_id')) {
                $allInputVal = $this->input->post();
                $cart_session = $_SESSION['cart_session'];
                $c = $this->db->query("select * from `cart` where `cart_session` = '$cart_session'")->row();
                $product_type = $c->product_type;
               
                $v = $this->db->select('*')->from('product')->where(array('product.id' => $c->product_id))->get()->row();
   
           
                if (count($allInputVal['product_id']) > 0) {
                    foreach ($allInputVal['product_id'] as $ProKey => $proId) {
                        if (!empty($proId)) {
                            $produtType = '';
                            if ($allInputVal['product_price_type'][$ProKey] == '') {
                                $produtType = 'Total';
                            } else {
                                $produtType = $allInputVal['product_price_type'][$ProKey];
                            }

                            if($product_type == 'BILL'){
                                $product_price = $allInputVal['product_price'][$ProKey];
                            }else{
                                // $p = $this->db->query("select `product_price` from `product` where `id` = '$proId'")->row();
                                // $product_price = $p->product_price;
                                $user = $this->aauth->get_user();
                                $product_price = $c->price;
                                $plan = $this->db->query("SELECT * FROM service_charges WHERE meter_no = '$user->meter_no'")->row();
                                if(!is_null($plan->payment_type_id)){
                                    $p_id = $plan->payment_type_id;
                                    $p = $this->db->query("SELECT * FROM payment_type WHERE id = '$p_id'")->row();
                                    $payment_plan = $p->occurrence;
                                }else{
                                   
                                    $plan = $this->db->query("SELECT * FROM service_charges INNER JOIN product ON product.id = service_charges.product_id 
                                        INNER JOIN payment_type ON payment_type.id = product.payment_type_id 
                                        WHERE service_charges.meter_no = '$user->meter_no'")->row();
                                    $payment_plan = $plan->occurrence;
                                }
                               
                                $quantity = $allInputVal['product_quantity'][$ProKey] / $payment_plan;
                            }
                   
                            $product = $this->db->query("update cart set price='" .$product_price * $quantity . "', quantity='" . $allInputVal['product_quantity'][$ProKey] . "', product_price_type='" . $produtType . "',payment_status = 'Pending',meter_no='" . $merNo . "', order_id = '" . $_SESSION['order_id'] . "' where cart_session='" . $_SESSION['cart_session'] . "' and user_id='" . $_SESSION['id'] . "' and  product_id='" . $proId . "'");

                        }
                    }
                } else {

                    return redirect('shopping/cart');

                }
            }

            $c_user = $this->aauth->get_user();
            $userId = $c_user->id;
            $totalAmount = array();
            $product = $this->db->query("select * from cart where user_id='" . $userId . "' AND payment_status = 'Pending' AND cart_session='" . $_SESSION['cart_session'] . "' AND order_id ='" . $_SESSION['order_id'] . "'")->result();

            $data['records'] = $product;
            $data['payment_type'] = '';
            $narration = '';
   
            foreach ($product as $pro) {
                $data['payment_type'] = $pro->product_type;
                if($pro->product_type == 'BILL' && $v->product_type == 'Power'){
                  
                    //$product_info = $this->db->query("SELECT * FROM product WHERE id = '" . $pro->product_id . "'")->row();

                   // if ($product_info->product_type == 'Power' || $product_info->product_type == 'Top Up') {
                        if (!empty($merNo)) {
                            // Change this back to  != 00
                            $code = $this->verifyVending($merNo);
                            
                            $minumum_purchase = (int)$code['minimum_purchase'];
                           
                            if ( $code['responseCode'] != '00') {
                                $this->session->set_flashdata('flashMsg', 'Meter could not be verified, please try again later.');
                                return redirect('shopping/cart');
                            }else if($pro->price <= $minumum_purchase){
                                $this->session->set_flashdata('errors', "Mimimum Purchase should be greater than: ".$minumum_purchase);
                                return redirect('shopping/cart');
                            }
                        } else {
                            $this->session->set_flashdata('flashMsg', 'Sorry meter number cannot be empty, plese update your meter number.');
                            return redirect('shopping/cart');
                        }
                   // }
                }else if($pro->product_type == 'BILL'){
                    $product_info = $this->db->query("SELECT * FROM outstanding_bills WHERE id = '" . $pro->product_id . "'")->row();
                }
                $totalAmount[] = $pro->price;//*$pro->quantity;
                $narration .= $pro->product_name . ' ';
            }

            $narration = (empty($narration)) ? 'Payment' : $narration;
            $ProPay = $this->db->query("select * from payments where user_id='" . $userId . "' AND status = 'Pending' AND  payment_id ='" . $_SESSION['order_id'] . "'")->result();


            $allProAmount = array_sum($totalAmount);
            $data['order_id'] = $_SESSION['order_id'];
            $data['narration'] = 'Payment For : ' . $narration;

            $pro = $this->db->query("SELECT * FROM product WHERE id = '" . $product[0]->product_id . "'")->row();
  
            if (count($ProPay) > 0) {
                $product = $this->db->query("update  payments set status = 'Pending' ,meter_no='" . $merNo . "' , amount ='" . $allProAmount . "' ,type='Service' where payment_id = '" . $_SESSION['order_id'] . "' and user_id='" . $_SESSION['id'] . "'");
            } else {
                $num_of_month = $this->input->post('product_quantity[0]');
                if($data['payment_type'] == 'BILL'){
                    $product = $this->db->query("INSERT INTO payments set status = 'Pending', quantity='".$num_of_month."', meter_no='" . $merNo . "' , payment_id = '" . $_SESSION['order_id'] . "', amount ='" . $allProAmount . "',user_id='" . $_SESSION['id'] . "' , date_created='" . date('Y-m-d H:i:s') . "',type='". $data['payment_type']."'");
                }else{
                    $product = $this->db->query("INSERT INTO payments set status = 'Pending', quantity='".$num_of_month."', meter_no='" . $merNo . "' , payment_id = '" . $_SESSION['order_id'] . "', amount ='" . $allProAmount . "',user_id='" . $_SESSION['id'] . "' , date_created='" . date('Y-m-d H:i:s') . "',type='$pro->product_type'");
                }
               
            }
          
            if($check_out === 'Pay with Card'){
            //    $data['charges'] = 1.4;
            //    $data['amount'] = round($allProAmount + (($allProAmount / 100) * 1.4) + 100);
                $data['amount'] = round($allProAmount);
            }else{
                //$data['charges'] = 1.25;
                //$data['amount'] = round($allProAmount + (($allProAmount / 100)));
               // $data['amount'] = round($allProAmount + (($allProAmount / 100) * 1.25) + 100);
               $data['amount'] = round($allProAmount);
            }
           
           //$data['amount'] = round($allProAmount);
            //$data['pfee']  =  array_sum($allProAmount)/100)*1.4
            if($data['payment_type'] == 'BILL'){
                $this->template->load('default', 'shopping/checkout_bill', $data);
            }else{
                $this->template->load('default', 'shopping/checkout', $data);
            }
        } else {
            return redirect('product/product');

        }

    }

    // private function vendLogin()
    // {
    //     $service_url = 'http://ebteller-api.prod.vggdev.com/api/token';
    //     $curl = curl_init($service_url);
    //     $curl_post_data = array(
    //         'username' => 'unwana.ekanem@netplusadvisory.com',
    //         'password' => 'Unwana1'
    //     );
    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($curl, CURLOPT_POST, true);
    //     curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
    //     $curl_response = curl_exec($curl);
    //     $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    //     curl_close($curl);
    //     if ($httpcode == 200) {
    //         $decoded = json_decode($curl_response);
    //         $accessToken = $decoded->access_token;
    //         return $accessToken;
    //     }
    //     return false;

    // }



    private function verifyVending($MeterNo)
    {

        // $MeterNo = 62317012086;
        // $endpoint = "http://41.216.166.165:8080/MEMMCOLWebServices_Pilot/webresources/IdentificationV2/102/$MeterNo";
        $endpoint = "http://41.216.166.165:8080/MEMMCOLWebServices/webresources/IdentificationV2/102/$MeterNo";

        $session = curl_init($endpoint);  
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);// return values as a string, not to std out
        
        $responsexml = curl_exec($session);                     // send the request
        $httpcode = curl_getinfo($session, CURLINFO_HTTP_CODE);
        $xml = simplexml_load_string($responsexml);
        $responsexml = json_encode($xml);

        curl_close($session);
        if ($httpcode == 200) {  
            $tokenDecode = json_decode($responsexml);   
            $data['responseCode'] = $tokenDecode->responsecode;
            $data['minimum_purchase'] = $tokenDecode->minimumPurchase;
            return $data;
        }
        return false;
    }


    //Test

    // public function verifyVending($MeterNo){
    //     $data['responseCode'] = '00';
    //     $data['minimum_purchase'] = 0;
    //     return $data;
    // }


    // private function generateTestToken()
    // {
    //     $c_user = $this->aauth->get_user();
    //     $MeterNo = $c_user->meter_no;
    //     $payment_id = uniqid('TEST');
    //     $loginToken = $this->vendLogin();
    //     if ($loginToken) {
    //         $vend = $this->vend($loginToken, $payment_id, $MeterNo, 1);
    //         if ($vend) {
    //             if ($vend->responsecode == 00)
    //                 return true;
    //         }
    //     }
    //     return false;
    // }

    private function generatePowerToken($Tokenamount, $payment_id)
    {
        $c_user = $this->aauth->get_user();
        $MeterNo = $c_user->meter_no;
        $payment_id = $payment_id;
        //$MeterNo = 62130072192;
        $vend = $this->vend($payment_id, $MeterNo, $Tokenamount);

        
        //$this->Product_model->update(['vend_log'=>print_r($vend)],$payment_id);
        if ($vend) {
            return $vend;
        }
        
        return false;
    }

    // Working Vending API

    public function vend($payment_id, $MeterNo, $Tokenamount){
       // $url = "http://41.216.166.165:8080/MEMMCOLWebServices_Pilot/webresources/PaymentV2/$MeterNo/prepaid/102/$payment_id/$Tokenamount";
        $url = "http://41.216.166.165:8080/MEMMCOLWebServices/webresources/PaymentV2/$MeterNo/prepaid/102/$payment_id/$Tokenamount";
        $ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $result = curl_exec($ch);

		if($result == false)
		{
			echo json_encode([curl_error($ch)]);
        }
        $xml = simplexml_load_string($result);
        $result = json_encode($xml);

        
		//$res = json_decode($result);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch); 
        if ($httpcode == 200) {
            $tokenDecode = json_decode($result);   
            $responseCode = $tokenDecode->responsecode;
   
            return $tokenDecode;
        }
        return false;
    }

    // public function vend($payment_id, $MeterNo, $Tokenamount){
    //     $dataToken = json_encode(array(
    //         'recieptNumber' => '4859 9945 9546',
    //         'responsecode' => '00', 
    //         'responsedesc' => '095869495',
    //         'token_amount' => '50',
    //         'paidamount' =>  $Tokenamount,
    //         'costOfUnit' => '500',
    //         'unit' => '10',
    //         'vat' => '60'
    //         ));
    //         $tokenDecode = json_decode($dataToken);
    //         return $tokenDecode;
        
    // }



    function shoppinglist()
    {

        if ($this->aauth->is_member('Admin')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');
            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('product.product_type', 'Service');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->order_by("payments.date_created", "desc")->get('payments');
        }
        if ($this->aauth->is_member('Public')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');
            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('product.product_type', 'Service');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->where('payments.user_id', $this->aauth->get_user()->id)->order_by("payments.date_created", "desc")->get('payments');

        }
        $recent_payments = $query->result();

        $data = array(
            'title' => 'My Payments',
            'payments' => $recent_payments,
            'date_from' => $this->input->post('date_from'),
            'date_to' => $this->input->post('date_to')
        );
        $this->template->load('default', 'shopping/shoppinglist', $data);

    }

    function otherpayments()
    {

        if ($this->aauth->is_member('Admin')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');
            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('product.product_type', 'Other');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->order_by("payments.date_created", "desc")->get('payments');
        }
        if ($this->aauth->is_member('Public')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');
            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('product.product_type', 'Other');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->where('payments.user_id', $this->aauth->get_user()->id)->order_by("payments.date_created", "desc")->get('payments');

        }
        $recent_payments = $query->result();

        $data = array(
            'title' => 'My Other Payments',
            'payments' => $recent_payments,
            'date_from' => $this->input->post('date_from'),
            'date_to' => $this->input->post('date_to')
        );
        $this->template->load('default', 'shopping/otherpayments', $data);

    }

    function topuppayment()
    {

        if ($this->aauth->is_member('Admin')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');
            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('product.product_type', 'Top Up');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->order_by("payments.date_created", "desc")->get('payments');
        }
        if ($this->aauth->is_member('Public')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');

            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('product.product_type', 'Top Up');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->where('payments.user_id', $this->aauth->get_user()->id)->order_by("payments.date_created", "desc")->get('payments');

        }
        $recent_payments = $query->result();
        $data = array(
            'title' => 'My Payments',
            'payments' => $recent_payments,
            'date_from' => $this->input->post('date_from'),
            'date_to' => $this->input->post('date_to')
        );
        $this->template->load('default', 'shopping/topuppayment', $data);


    }


    function adminpowerpayments()
    {

        if ($this->aauth->is_member('Admin')) {
            $query =  $this->Payment_model->getAdminPayment();
          
        }
        if ($this->aauth->is_member('Public')) {
 
            $user_id =  $this->aauth->get_user()->id;
            $query = $this->Payment_model->getAdminPaymentByUserId($user_id);
        }
     
        $data = array(
            'title' => 'My Payments',
            'payments' => $query,
            'date_from' => $this->input->post('date_from'),
            'date_to' => $this->input->post('date_to')
        );
        $this->template->load('default', 'shopping/adminpowerpayments', $data);


    }


    function securitychargepayment()
    {

        if ($this->aauth->is_member('Admin')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');
            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('product.product_type', 'Security Charge');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->order_by("payments.date_created", "desc")->get('payments');
        }
        if ($this->aauth->is_member('Public')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');

            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('product.product_type', 'Security Charge');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->where('payments.user_id', $this->aauth->get_user()->id)->order_by("payments.date_created", "desc")->get('payments');

        }
        $recent_payments = $query->result();
        $data = array(
            'title' => 'My Payments',
            'payments' => $recent_payments,
            'date_from' => $this->input->post('date_from'),
            'date_to' => $this->input->post('date_to')
        );
        $this->template->load('default', 'shopping/securitychargepayment', $data);


    }

    public function servicechargedept_edit($id){
        $meter_no = $id;
         $r=  $this->db->query("SELECT * FROM service_charges Inner Join aauth_users On service_charges.user_id = aauth_users.id where service_charges.meter_no = '" . $meter_no . "' ")->row();
      
        $data['meter_no'] = $meter_no;
        $data['user_id'] = $r->user_id;
        $data['full_name'] = $r->full_name;
        $data['amount_due'] = $r->amount_due;
        $data['total_paid'] = $r->total_paid;
        $data['advance_payment'] = $r->advance_payment;
        $data['advance_month'] = $r->advance_month;

        $this->template->load('default', 'shopping/editservicecharge', $data);
        
    }

    public function addservicecharge(){
        $meter_no = $this->input->post('meter_no');
        $amount_due = $this->input->post('amount_due');
        $advance_payment = $this->input->post('advance_payment');
        $advance_month = $this->input->post('advance_month');

        $update = $this->db->query("UPDATE service_charges SET amount_due = '$amount_due', advance_payment = '$advance_payment', advance_month = '$advance_month' where meter_no = '$meter_no'");

        
        // var_dump($this->db->last_query());
        // die;
        
        $this->session->set_flashdata('success', 'Payment Completed successful.');
        redirect('shopping/servicechargedebt');
    }


    function waterpumppayment()
    {

        if ($this->aauth->is_member('Admin')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');
            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('product.product_type', 'Water Pump');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->order_by("payments.date_created", "desc")->get('payments');
        }
        if ($this->aauth->is_member('Public')) {
            $this->db->select('*,payments.date_created as payment_date');
            $this->db->join('aauth_users', 'aauth_users.id = payments.user_id');
            $this->db->join('cart', 'cart.order_id = payments.payment_id');
            $this->db->join('product', 'product.id = cart.product_id');

            if (!empty($this->input->post('date_from')) && !empty($this->input->post('date_to'))) {
                $dateFrom = date('Y-m-d', strtotime($this->input->post('date_from')));// .'00:00:00';
                $dateTo = date('Y-m-d', strtotime($this->input->post('date_to') . '+1 day'));// .'23:59:59';
                $this->db->where('payments.date_created >=', $dateFrom);
                $this->db->where('payments.date_created <=', $dateTo);

            }
            $this->db->where('product.product_type', 'Water Pump');
            $this->db->group_by('cart.cart_id');
            $this->db->group_by('payments.id');
            $this->db->group_by('cart.order_id');
            $query = $this->db->where('payments.user_id', $this->aauth->get_user()->id)->order_by("payments.date_created", "desc")->get('payments');

        }
        $recent_payments = $query->result();
        $data = array(
            'title' => 'My Payments',
            'payments' => $recent_payments,
            'date_from' => $this->input->post('date_from'),
            'date_to' => $this->input->post('date_to')
        );
        $this->template->load('default', 'shopping/waterpumppayment', $data);


    }


    function servicechargedebt()
    {

        if ($this->aauth->is_member('Admin')) {

            $this->db->select('*');
            $this->db->join('aauth_users', 'service_charges.user_id = aauth_users.id');
            $this->db->join('product', 'product.id = service_charges.product_id');
            $query = $this->db->order_by("service_charges.id", "desc")->get('service_charges');
            $charges = $query->result();

            $data = array(
                'title' => 'Credit',
                'charges' => $charges,
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to')
            );
        }
        $this->template->load('default', 'shopping/servicechargedebt', $data);
    }


    function GenerateOrder($length)
    {
        $code = "";
        $possible = "012346789";
        $maxlength = strlen($possible);
        if ($length > $maxlength) {
            $length = $maxlength;
        }
        $i = 0;
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, $maxlength - 1), 1);
            if (!strstr($code, $char)) {
                $code .= $char;
                $i++;
            }
        }
        return $code;
    }


    function pay_success($orderId)
    {
        $userDetails = $this->aauth->get_user();
        $data['is_power'] = 0;
        $cart = $this->db->query("select * from cart where user_id='" . $userDetails->id . "' AND order_id ='" . $orderId . "'")->row();
        $product_info = $this->db->query("SELECT product.* FROM product Inner Join cart On product.id = cart.product_id where cart.order_id = '" . $orderId . "' ")->row();
        if($cart->product_type == 'BILL' && $product_info->product_type != 'Power' ){
            $product_info = $this->db->query("SELECT outstanding_bills.* FROM outstanding_bills Inner Join cart On outstanding_bills.id = cart.product_id where cart.order_id = '" . $orderId . "' ")->row();
            $data['product_name'] = $product_info->description;
        }else {
            $product_info = $this->db->query("SELECT product.* FROM product Inner Join cart On product.id = cart.product_id where cart.order_id = '" . $orderId . "' ")->row();
            if ($product_info->product_type == 'Power' || $product_info->product_type == 'Top Up') {
                $data['is_power'] = 1;
            }
            $data['product_name'] = $product_info->product_name;
        }
        $OrderDetails = $this->db->query("select * from payments where   payment_id ='" . $orderId . "'")->row();

        $data['full_name'] = $userDetails->full_name;
        $data['tr_date'] = date('Y-M-d', strtotime($OrderDetails->date_created));
        $data['tr_time'] = date('H:i', strtotime($OrderDetails->date_created));;
        $data['order_id'] = $orderId;
        $data['amount_paid'] = $OrderDetails->amount_paid;
        $data['token_no'] = $OrderDetails->token_no;
        $data['token_desc'] = $OrderDetails->token_desc;
        $data['token_amount'] = $OrderDetails->token_amount;

        $data['title'] = 'Payment successful';
        $this->template->load('default', 'shopping/pay-success', $data);

    }


    function pay_failed($orderId)
    {

        $userDetails = $this->aauth->get_user();

        $OrderDetails = $this->db->query("select * from payments where   payment_id ='" . $orderId . "'")->row();
        $data['product_name'] = '';
        $data['full_name'] = $userDetails->full_name;
        $data['tr_date'] = date('Y-M-d', strtotime($OrderDetails->date_created));
        $data['tr_time'] = date('H:i', strtotime($OrderDetails->date_created));;
        $data['order_id'] = '';
        $data['amount_paid'] = '';
        $data['token_no'] = '';
        $data['token_amount'] = '';
        $data['token_desc'] = '';
        $data['reason'] = 'Invalid order';
        $data['is_power'] = 0;
        if ($OrderDetails) {
            $data['is_power'] = 0;
            $cart = $this->db->query("select * from cart where user_id='" . $userDetails->id . "' AND order_id ='" . $orderId . "'")->row();
            $product_info = $this->db->query("SELECT product.* FROM product Inner Join cart On product.id = cart.product_id where cart.order_id = '" . $orderId . "' ")->row();
            if($cart->product_type == 'BILL'  && $product_info->product_type != 'Power'){
                $product_info = $this->db->query("SELECT outstanding_bills.* FROM outstanding_bills Inner Join cart On outstanding_bills.id = cart.product_id where cart.order_id = '" . $orderId . "' ")->row();
                $data['product_name'] = $product_info->description;
            }else {
                $product_info = $this->db->query("SELECT product.* FROM product Inner Join cart On product.id = cart.product_id where cart.order_id = '" . $orderId . "' ")->row();
                if ($product_info->product_type == 'Power' || $product_info->product_type == 'Top Up') {
                    $data['is_power'] = 1;
                }
                $data['product_name'] = $product_info->product_name;
            }
            $data['full_name'] = $userDetails->full_name;
            $data['tr_date'] = date('Y-M-d', strtotime($OrderDetails->date_created));
            $data['tr_time'] = date('H:i', strtotime($OrderDetails->date_created));;
            $data['order_id'] = $orderId;
            $data['amount_paid'] = $OrderDetails->amount_paid;
            $data['token_no'] = $OrderDetails->token_no;
            $data['token_desc'] = $OrderDetails->token_desc;
            $data['token_amount'] = $OrderDetails->token_amount;
            $data['reason'] = $OrderDetails->payment_description;
        }


        $data['title'] = 'Payment Failed';
        $this->template->load('default', 'shopping/pay-failed', $data);

    }

    public function payment_response()
    {
        //Current user data
        $c_user = $this->aauth->get_user();
        $userId = $c_user->id;
        $MeterNo = $c_user->meter_no;

       // var_dump($this->input->get());
        //Response data
        $code = $this->input->get('code', true);
        $trans_id = $this->input->get('trans_id', true);
        $order_id = $this->input->get('orderid', true);
        $amount = $this->input->get('amount', true);
        $result = $this->input->get('result', true);
        $message = $this->input->get('message', true);
        $bank = $this->input->get('bank', true);
        $order_id = str_replace(' ', '', $order_id);

        $Maildata = array();
        $Maildata['full_name'] = $c_user->full_name;
        $Maildata['to'] = $c_user->email;
        $Maildata['order_id'] = $order_id;
        $Maildata['trans_ref'] = $trans_id;
        $Maildata['bank'] = $bank;
        $Maildata['amount_paid'] = $amount;
        $Maildata['reason'] = $message;
        $Maildata['tr_date'] = date('d/m/YYYY');
        $Maildata['tr_time'] = date('H:i');
        $Maildata['token_no'] = '';
        $Maildata['token_desc'] = '';
        $Maildata['token_amount'] = '';
        $Maildata['is_power'] = 0;
        $Maildata['product_name'] = '';
        if (empty($code) || empty($trans_id) || empty($order_id) || empty($result) || empty($message)) {
            $error = 'Missing parameters.';
        } else {
            $data = array(
                'transaction_id' => $trans_id,
                'bank' => $bank,
                'amount_paid' => $amount,
                'payment_description' => $message,
                'payment_id' => $order_id
            );
            $payment_verify = $this->Payment_model->get(array('payment_id' => $order_id));
            if ($payment_verify) {
                if ($payment_verify && $payment_verify->status == 'Pending') {
                    if ($code == '00') {
                        $r_ver_data = ['order_id' => $order_id, 'trans_id' => $trans_id];

                      //  if ($this->verifyNetplusPayment($payment_verify, $r_ver_data)) {
                            $product_info = $this->db->query("SELECT product.* FROM product Inner Join cart On product.id = cart.product_id where cart.order_id = '" . $order_id . "' ")->row();
                            $data['type'] =  $product_info->product_type;
                            $data['status'] = 'Paid';
                            $Maildata['payment_status'] = 'Paid';
                            $this->Product_model->update($data, $payment_verify->id);
                            
                            $meter_no = $this->Product_model->getDetails($c_user->meter_no);
                            $amount_due = (int)$meter_no[0]->amount_due;
                            $amount = (int)$amount;
                            $cart = $this->db->query("select * from cart where user_id='" . $userId . "' AND order_id ='" . $order_id . "'")->row();


                            // if(isset($c_user->id) && $amount_due > 0){
                            //     $amount = $amount_due  - $amount_due ;
                            //     $totalpaid = $user_id[0]->total_paid + $amount_due ;
                            //     $this->Product_model->updateServiceCharge($c_user->id, $amount, $totalpaid);
                            // }
                            //else{
                            //     $this->Product_model->insertServiceCharge($c_user->id, $MeterNo,  $user_id[0]->amount_due,  $user_id[0]->amount_due);
                                
                            // }

                            $plan = $this->db->query("SELECT * FROM service_charges WHERE meter_no = '$user->meter_no'")->row();
                            if(!is_null($plan->payment_type_id)){
                                $p_id = $plan->payment_type_id;
                                $p = $this->db->query("SELECT * FROM payment_type WHERE id = '$p_id'")->row();
                                $payment_plan = $p->occurrence;
                            }else{
                               
                                $plan = $this->db->query("SELECT * FROM service_charges INNER JOIN product ON product.id = service_charges.product_id 
                                INNER JOIN payment_type ON payment_type.id = product.payment_type_id 
                                WHERE service_charges.meter_no = '$c_user->meter_no'")->row();
                                $payment_plan = $plan->occurrence;
                            }
                            
                            if($cart->product_type == 'BILL' && $product_info->product_type == 'Power'){
                        
                                $Maildata['product_name'] = $product_info->product_name;
                                if($product_info->product_type == 'Power'){
                                    $Maildata['is_power'] = 1;
                                    $Tokenamount = $amount;
                                    //$Tokenamount = $amount;
                                    //$Tokenamount = ($product_info->product_type == 'Top Up') ? $topupamount : $product_info->take_or_pay;
                                    //$xmlrequest = array('transactionId' => $order_id, 'meterNumber' => $MeterNo, 'amount' => $Tokenamount);
                                    $powerToken = $this->generatePowerToken($Tokenamount, $order_id);
                                    
                                    //"Vendin api call after send token';
                                    $this->Product_model->update(['vend_log'=>json_encode($powerToken)], $payment_verify->id);
                                    if ($powerToken) {
                                        if ($powerToken->responsecode == '00') {
                                            $Maildata['token_no'] = $powerToken->recieptNumber;
                                            $dataToken = array('token_no' => $powerToken->recieptNumber, 'token_desc' => $powerToken->responsedesc, 'token_amount' => $powerToken->unit);
                                            $this->Product_model->update(['vend_log'=>json_encode($dataToken)], $payment_verify->id);
                                            $this->Product_model->update($dataToken, $payment_verify->id);

                                        } else {

                                            $dataToken = array(
                                                
                                                'token_desc' => $powerToken->responsedesc,
                                                'token_amount' => $powerToken->unit,
                                                   
                                            );
                                            // $dataToken = array('token_no' => $powerToken->recieptNumber, 'token_desc' => $powerToken->responsedesc, 'token_amount' => $powerToken->unit);
                                            $this->Product_model->update(['vend_log'=>json_encode($powerToken)], $payment_verify->id);
                                            $this->Product_model->update($dataToken, $payment_verify->id);
                                        }
                                        $Maildata['token_desc'] = $powerToken->responsedesc;
                                        $Maildata['token_amount'] = $powerToken->unit;
                                    }
                                }
                            }else if($cart->product_type == 'BILL'){
                                $product_info = $this->db->query("SELECT outstanding_bills.* FROM outstanding_bills Inner Join cart On outstanding_bills.id = cart.product_id where cart.order_id = '" . $order_id . "' ")->row();
                                $Maildata['product_name'] = $product_info->description;
                                $this->Outstanding_model->update(['status'=>1,'order_id'=>$order_id], $cart->product_id);
                            }else{
                                $product_info = $this->db->query("SELECT product.*, cart.quantity FROM product Inner Join cart On product.id = cart.product_id where cart.order_id = '" . $order_id . "' ")->row();
                                $Maildata['product_name'] = $product_info->product_name;
                               
                               if($product_info->product_type == 'Service'){ 
                                    //Get the payment plan then add it to the created_date

                                    //Get created date
                                    $c = $this->db->query("SELECT * FROM service_charges where meter_no = '$MeterNo'")->row();
                                 
                                    $amount_due = (int)$c->amount_due - (int)$amount;
                                    $totalpaid = $meter_no[0]->total_paid +  $amount;
                                    $advance_month= 0;
                                    $created_at = $c->date_created;
                                    $date = strtotime($created_at);
                                    
                                    $quantity =  $product_info->quantity;
                                    
                                    $qan = (int)$quantity / (int)$payment_plan;
                                    $num = (int)$payment_plan * $qan;
                                    $number = $num - 1;

                                    $date = strtotime(date("Y-m-d",  $date) . "+" .$number ."  month"); 
                                    $date = date("Y-m-d",$date); 


                                    $updated_date =  date('Y-m-d', strtotime('+30 day', strtotime($date)));
                                    

                                    $this->Product_model->updateServiceCharge($MeterNo, $amount_due, $totalpaid,  $advance_month, $updated_date);
                               
                                }
                            }
                            $this->session->set_flashdata('success', 'Payment Completed successful.');
                            //Send payment email
                            $this->send_payment_email($Maildata);

                            unset($_SESSION['order_id']);
                            unset($_SESSION['cart_session']);

                            redirect('shopping/pay_success/' . $order_id);
                            exit;
                        // } else {
                        //     $error = 'Payment verification failed.';
                        // }
                    }
                    if ($code !== '00') {
                        $data['status'] = 'Payment Failed';
                        $error = $message;
                    }
                } else {
                    $error = 'Order payment status ('.$payment_verify->status.') invalid.';
                }
                $data['payment_description'] = $error;
                $this->Product_model->update($data, $payment_verify->id);
            }
        }
        unset($_SESSION['order_id']);
        unset($_SESSION['cart_session']);
        redirect('shopping/pay_failed/' . $order_id);
    }

    private function send_payment_email($Maildata)
    {
        $to = $Maildata['to'];
        $subject = 'Payment Completed successful';
        $config = array(
            'mailtype' => 'html',
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_user' => 'saddle@netplusadvisory.com',
            'smtp_pass' => 'Saddle7890',
            'smtp_port' => 465,
            'priority' => 1,
            'newline' => "\r\n"
        );
        var_dump($Maildata);
        $message = $this->load->view('shopping/pay-success.php', $Maildata, true);
        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('saddle@netplusadvisory.com', 'Richmond Gate Phase 1 Estate Association');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $r = $this->email->send();
    }

    private function verifyNetplusPayment($payment_verify, $r_ver_data)
    {
       //$service_url = 'http://api.netpluspay.com/transactions/requery/MID5a3b9f942b3a56.21053140/' . $r_ver_data['trans_id'];
       //$service_url = 'http://api.netpluspay.com/transactions/requery/TEST5a3b9e69230c1/' . $r_ver_data['trans_id'];
       $service_url = 'http://api-test.netpluspay.com/transactions/requery/TEST5a3b9e69230c1/' . $r_ver_data['trans_id'];
       $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_REFERER, $service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        
        if ($httpcode == 200) {
            $decoded = json_decode($curl_response);
            $res_data = $decoded;
            if ((strtolower($res_data->status) == 'captured' || strtolower($res_data->status) == 'authorized') && ($res_data->orderid == $payment_verify->payment_id))
                return true;
        }
        return false;
    }
}
