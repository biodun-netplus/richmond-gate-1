<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_model');
        $this->load->model('Product_model');
        $this->load->model('Outstanding_model');
        if (!$this->aauth->is_loggedin())
            redirect('login', 'refresh');
        if (!$this->aauth->is_member('Admin') && !$this->aauth->is_member('Merchant') && !$this->aauth->is_member('Public'))
            show_error('Access Denied');

    }

    public function index()
    {
        $data = array(
            'title' => 'Product'
        );
        $TableNAme = 'product';
        $data['records'] = $this->Product_model->getRecord($TableNAme);
        $this->template->load('default', 'product/admin/index', $data);

    }


    function product()
    {

        $payments = 0;
        $total_payments = 0;
        $last_payment = 0;
        $users = 0;
        $c_user = $this->aauth->get_user();
        $recent_payments = array();
        $productpower = '';
        $productservices = '';
        $productwaterpump = '';
        $productsecuritycharge = '';
        $productTopup = '';
        $TopupActivate = '';
        $dueServiceAmount = 0;
        $duePowerAmount = 0;
        $servicePayments = 0;
        $outstanding=[];

        //d($c_user);
        //die;
        if ($this->aauth->is_member('Public')) {


            $user = $this->aauth->get_user();

            $productpower = $this->db->query("SELECT * FROM product where partner_type='" . $user->partner_type . "' and product_type='Power' AND   property_type='" . $user->type_of_property . "' ")->result();

            // Total amount due power
            $TotalAmountForPowerPayment = $this->db->query("SELECT sum(product_price) as amountTotal
														FROM product
														WHERE product_type='Power' AND partner_type = '" . $user->partner_type . "' AND property_type = '" . $user->type_of_property . "'")->row();
            $duePowerAmount = $TotalAmountForPowerPayment->amountTotal;

           
            $productservices = $this->db->query("SELECT * FROM `service_charges` INNER JOIN `product` on `service_charges`.`product_id` = `product`.`id` WHERE `meter_no` = '". $user->meter_no."'")->result();

            $productwaterpump = $this->db->query("SELECT * FROM product where product_type='Water Pump' and property_type='" . $user->type_of_property . "'  and partner_type='" . $user->partner_type . "'  ")->result();
            $productsecuritycharge =  $this->db->query("SELECT * FROM product where product_type='Security Charge' and property_type='" . $user->type_of_property . "'  and partner_type='" . $user->partner_type . "'  ")->result();
            $productOther = $this->db->query("SELECT * FROM product where product_type='Other'  and property_type='" . $user->type_of_property . "' and partner_type='" . $user->partner_type . "' ")->result();
           // $productOther = $this->db->query(" SELECT * FROM `product` INNER JOIN `cart` on `product`.`id` = `cart`.`product_id` INNER JOIN `payments` on `cart`.`order_id` = `payments`.`payment_id` where `product`.`product_type`='Other' and property_type='" . $user->type_of_property . "' and partner_type='" . $user->partner_type . "' ")->result();
          
            // Total amount due service
            $TotalAmountForServicePayment = $this->db->query("SELECT sum(product_price) as amountTotal
														FROM product
														WHERE product_type='Service' AND partner_type = '" . $user->partner_type . "' AND property_type = '" . $user->type_of_property . "'")->row();
            $dueServiceAmount = $TotalAmountForServicePayment->amountTotal;
            $serviceDue = $dueServiceAmount;


            // Total amount due Water
            $TotalAmountForServicePayment = $this->db->query("SELECT sum(product_price) as amountTotal
														FROM product
														WHERE product_type='Water Pump' AND partner_type = '" . $user->partner_type . "' AND property_type = '" . $user->type_of_property . "'")->row();
            $dueWaterAmount = $TotalAmountForServicePayment->amountTotal;
            if($dueWaterAmount == NULL){
                $dueWaterAmount = 0;
            }
           

             // Total amount security
             $TotalAmountForServicePayment = $this->db->query("SELECT sum(product_price) as amountTotal
                        FROM product
                        WHERE product_type='Security Charge' AND partner_type = '" . $user->partner_type . "' AND property_type = '" . $user->type_of_property . "'")->row();
            $dueSecurityAmount = $TotalAmountForServicePayment->amountTotal;
            if($dueSecurityAmount == NULL){
                $dueSecurityAmount = 0;
            }

            //Get payment plan

            //Check if the service_charges payment plan is set
            $plan = $this->db->query("SELECT * FROM service_charges WHERE meter_no = '$user->meter_no'")->row();
            if(!is_null($plan->payment_type_id)){
                $p_id = $plan->payment_type_id;
                $p = $this->db->query("SELECT * FROM payment_type WHERE id = '$p_id'")->row();
                $payment_plan = $p->occurrence;
            }else{
               
                $plan = $this->db->query("SELECT * FROM service_charges INNER JOIN product ON product.id = service_charges.product_id 
                INNER JOIN payment_type ON payment_type.id = product.payment_type_id 
                WHERE service_charges.meter_no = '$user->meter_no'")->row();
                $payment_plan = $plan->occurrence;
            }

       
           //var_dump($payment_plan);die;
            

          
            // $product_info = $this->db->query("SELECT product.*, cart.* FROM product Inner Join cart On product.id = cart.product_id where cart.order_id = '55082971634' ")->row();
            // $quantity =  $product_info->quantity;
            // $qan = (int)$quantity / (int)$payment_plan;
            // $num = (int)$payment_plan * $qan;
            // $number = $num - 1;
            // var_dump($number);
            // $date = strtotime("2019-02-01");
            // $date = strtotime(date("Y-m-d",  $date) . "+" .$number ."  month"); 
            // $date = date("Y-m-d",$date); 
            // var_dump($date);
           
            
            // $updated_date =  date('Y-m-d', strtotime('+30 day', strtotime($date)));
            // var_dump($updated_date);
            // die;


            $month_start = strtotime('first day of this month', time());
            $month_end = strtotime('last day of this month', time());
            $startDate = date('Y-m-d', $month_start) . ' 00:00:00';
            $endDate = date('Y-m-d', $month_end) . ' 23:59:59';
            if($payment_plan == 12){
                $prev_month_start = strtotime("first day of -12 Months", time());
                $prev_month_end = strtotime('last day of previous month', time());
                $prevStartDate = date('Y-m-d', $prev_month_start) . ' 00:00:00';
                $prevEndDate = date('Y-m-d', $prev_month_end) . ' 23:59:59';
            }else if($payment_plan == 6){
                $prev_month_start = strtotime("first day of -6 Months", time());
                $prev_month_end = strtotime('last day of previous month', time());
                $prevStartDate = date('Y-m-d', $prev_month_start) . ' 00:00:00';
                $prevEndDate = date('Y-m-d', $prev_month_end) . ' 23:59:59';
            }else if($payment_plan == 3){
                $prev_month_start = strtotime("first day of -3 Months", time());
                $prev_month_end = strtotime('last day of previous month', time());
                $prevStartDate = date('Y-m-d', $prev_month_start) . ' 00:00:00';
                $prevEndDate = date('Y-m-d', $prev_month_end) . ' 23:59:59';
            }else if($payment_plan == 2){
                $prev_month_start = strtotime("first day of -2 Months", time());
                $prev_month_end = strtotime('last day of previous month', time());
                $prevStartDate = date('Y-m-d', $prev_month_start) . ' 00:00:00';
                $prevEndDate = date('Y-m-d', $prev_month_end) . ' 23:59:59';
            }else{
                $prev_month_start = strtotime('first day of previous month', time());
                $prev_month_end = strtotime('last day of previous month', time());
                $prevStartDate = date('Y-m-d', $prev_month_start) . ' 00:00:00';
                $prevEndDate = date('Y-m-d', $prev_month_end) . ' 23:59:59';
            }
 
            $date = date('Y-m-d h:i:s', time());
            $query = $this->db->query("SELECT * FROM `service_charges` WHERE `meter_no` = '$user->meter_no' AND `date_created` BETWEEN '$prevStartDate' and '$prevEndDate'");
            $dueServiceCharge = $query->result();
        

            if(!empty($dueServiceCharge)){
               
                if($dueServiceCharge[0]->amount_due > 0)
                {
                    $plan = 12 / $payment_plan;
                    $dueServiceAmount =  $dueServiceAmount / $plan;
                }else{
                    //Deduct from advance payment 
                    // if($dueServiceCharge[0]->advance_month > 0){
                    //     $advance_month = (int)$dueServiceCharge[0]->advance_month;
                    //     $advance_payment = (int)$dueServiceCharge[0]->advance_payment;
                    //     $total_paid = (int)$dueServiceCharge[0]->total_paid;

                    //     $amount_due = $advance_payment / $advance_month;
                    //     $total_paid = $total_paid + $amount_due;
                    //     $advance_month = $advance_month - 1;
                    //     $advance_payment = $advance_payment - $amount_due;
                    //     $dueServiceAmount = 0;
                    //     $this->db->query("UPDATE `service_charges` SET `amount_due` = '$dueServiceAmount', `total_paid` = '$total_paid',
                    //             `advance_payment` = '$advance_payment', `advance_month` = '$advance_month',  `date_created` = '$date' WHERE `meter_no` = '$user->meter_no'");
                    // }else{
                    //     $this->db->query("UPDATE `service_charges` SET `amount_due` = '$dueServiceAmount', `date_created` = '$date' WHERE `meter_no` = '$user->meter_no'");
                    // }
                   
                }
            }else{
               
                $plan = 12 / $payment_plan;
                $dueServiceAmount =  $dueServiceAmount / $plan;
             
            }


     
           
            $productTopup = $this->db->query("SELECT * FROM product where product_type='Top up' ")->result();
            //d($productservices);
            //die;
            // echo "SELECT * FROM payments where date_created BETWEEN '".$startDate."' and '". $endDate."' and user_id='".$user->id."' ";die;
            $TopupActivate = $this->db->query("SELECT sum(amount) as totalpaid FROM payments where status = 'Paid' AND date_created BETWEEN '" . $startDate . "' and '" . $endDate . "' and meter_no='" . $user->meter_no . "' ")->row();

            //service payments
            $servicePayments = $this->db->query("SELECT sum(amount) as totalpaid FROM payments Inner Join cart On payments.payment_id = cart.order_id INNER JOIN product on cart.product_id = product.id where payments.status = 'Paid' AND payments.date_created BETWEEN '" . $startDate . "' and '" . $endDate . "'  and payments.type = '" .'Service'."'  and payments.meter_no='" . $user->meter_no . "' ")->row();
            if($servicePayments->totalpaid == NULL){
                $servicePayments->totalpaid = 0;
            }

            $current_date = date('Y-m-d h:i:s', time());
        
            //get start date of service_charges
            $getServiceChargeDate = $this->db->query("SELECT * FROM service_charges WHERE meter_no = '".$user->meter_no."'")->row();
            $serviceChargeDate = $getServiceChargeDate->date_created;
      
            if($serviceChargeDate >= $current_date ){
                $serviceChargeDate = true;
            }else{
                $serviceChargeDate = false;
            }


            //water payments
            $waterPayments = $this->db->query("SELECT sum(amount) as totalpaid FROM payments Inner Join cart On payments.payment_id = cart.order_id INNER JOIN product on cart.product_id = product.id where payments.status = 'Paid' AND payments.date_created and payments.meter_no='" . $user->meter_no . "' and payments.type = '" .'Water Pump'. "' ")->row();
            if($waterPayments->totalpaid == NULL){
                $waterPayments->totalpaid = 0;
            }

            
            //security payments
            $securityPayments = $this->db->query("SELECT sum(amount) as totalpaid FROM payments Inner Join cart On payments.payment_id = cart.order_id INNER JOIN product on cart.product_id = product.id where payments.status = 'Paid' AND payments.date_created  and payments.meter_no='" . $user->meter_no . "' and payments.type = '" .'Security Charge'. "'")->row();
            if($securityPayments->totalpaid == NULL){
                $securityPayments->totalpaid = 0;
            }

            //service payments
            $lightPayments = $this->db->query("SELECT sum(amount) as totalpaid FROM payments Inner Join cart On payments.payment_id = cart.order_id INNER JOIN product on cart.product_id = product.id where payments.status = 'Paid' AND payments.date_created BETWEEN '" . $startDate . "' and '" . $endDate . "'  and payments.type = '" .'Power'."'  and payments.meter_no='" . $user->meter_no . "' ")->row();
            if($lightPayments->totalpaid == NULL){
                $lightPayments->totalpaid = 0;
            }


            //other payments
            // $otherPayments =  $this->db->query("SELECT sum(amount) as totalpaid FROM payments Inner Join cart On payments.payment_id = cart.order_id INNER JOIN product on cart.product_id = product.id where payments.status = 'Paid' AND payments.date_created  and payments.meter_no='" . $user->meter_no . "' and payments.type = '" .'Other'. "'")->row();
            // if($otherPayments->totalpaid == NULL){
            //     $otherPayments->totalpaid = 0;
            // }

            $otherPayments = $this->db->query(" SELECT * FROM `product` INNER JOIN `cart` on `product`.`id` = `cart`.`product_id` INNER JOIN `payments` on `cart`.`order_id` = `payments`.`payment_id` where `product`.`product_type`='Other' and `payments`.`status` = 'paid' and  property_type='" . $user->type_of_property . "' and partner_type='" . $user->partner_type . "' ")->result();

           
            $payments = $this->db->count_all_results('payments');
            $query = $this->db->select_sum('amount_paid')->where('status', 'Paid')->get('payments');
            $row = $query->row();
            $total_payments = $row->amount_paid;

            $users = count($this->aauth->list_users('Merchant'));
            $recent_payments = $this->payment_model->all(3);

            $outstanding = $this->Outstanding_model->user_outstanding(array('meter_no' => $c_user->meter_no,'status'=>0));            
        }

        // var_dump($dueServiceAmount);
        // var_dump($servicePayments);die;

        
        $data = array(
            'title' => 'Bills',
            'payments' => $payments,
            'total_payments' => $total_payments,
            'last_payment' => $last_payment,
            'users' => $users,
            'recent_payments' => $recent_payments,
            'productpower' => $productpower,
            'dueServiceAmount' => $dueServiceAmount,
            'serviceDue' => $serviceDue,
            'dueWaterAmount' => $dueWaterAmount,
            'dueSecurityAmount' => $dueSecurityAmount,
            'duePowerAmount' => $duePowerAmount,
            'dueAmount' => $duePowerAmount + $dueServiceAmount,
            'productservices' => $productservices,
            'productwaterpump' => $productwaterpump,
            'productsecuritycharge' => $productsecuritycharge,
            'servicePayments'=>$servicePayments,
            'serviceChargeDate' => $serviceChargeDate,
            'waterPayments' => $waterPayments,
            'securityPayments' => $securityPayments,
            'otherPayments' => $otherPayments,
            'lightPayments' => $lightPayments,
            'productother' => $productOther,
            'productTopup' => $productTopup,
            'TopupActivate' => $TopupActivate,
            'outstanding'=>$outstanding,

        );

        $this->template->load('default', 'product/admin/product', $data);

    }


    function product_details($productId)
    {


        $data['title'] = 'Product Details';
        $c_user = $this->aauth->get_user();
        $data['ProductDetails'] = $this->db->query("SELECT * FROM product where id='" . $productId . "' ")->row_array();
        $this->template->load('default', 'product/admin/product_details', $data);

    }

    public function product_add($action = NULL, $id = NULL)
    {
        $time = date('Y-m-d H:i:s');
        $data = array(
            'title' => 'Product - Add'
        );
        if (isset($action) && $action == 'add') {
            $tableName = 'meter_tariff';
            $data['meterTariff'] = $this->Product_model->getRecord($tableName);
            $data['actionType'] = 'add';
            if ($this->input->post('save') == 'Save') {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('product_name', 'Product Name', 'trim|required');
                $this->form_validation->set_rules('partner_type', 'Partner Name', 'trim|required');
                $this->form_validation->set_rules('account_number', 'Account Number', 'required');
                $this->form_validation->set_rules('bank_name', 'Bank Name', 'required');
                $this->form_validation->set_rules('payment_occurrence', 'Payment Occurrence', 'required');

                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('flashMsg', 'Fill All the field.');
                    redirect('product/product_add/add/');
                } else {
                    $InsertDataArray = array();
                    $InsertDataArray['product_name'] = $this->input->post('product_name');
                    if (!empty($this->input->post('product_price'))) {
                        $InsertDataArray['product_price'] = $this->input->post('product_price');
                        $data['product_price'] = $InsertDataArray['product_price'] ;
                    } 
                  
                    $InsertDataArray['payment_type_id'] = $this->input->post('payment_occurrence');
                    $InsertDataArray['partner_type'] = $this->input->post('partner_type');
                    $InsertDataArray['property_type'] = $this->input->post('property_type');
                    $InsertDataArray['product_type'] = $this->input->post('product_type');
                    $InsertDataArray['account_number'] = $this->input->post('account_number');
                    $InsertDataArray['bank_name'] = $this->input->post('bank_name');
                    $InsertDataArray['created'] = $time;

                   
                    $stts = $this->Product_model->setRecord('product', $InsertDataArray);
       
                    $data['type_of_property'] = $this->input->post('property_type');
                    $propertyType = $this->Product_model->getAllUsersWithPropertyType($data['type_of_property']);

                    
                    $count = count($propertyType);
                    for($i = 0; $i < $count; $i++){
                        $this->Product_model->insertServiceCharge($propertyType[$i]->id, $propertyType[$i]->meter_no,  $stts,  $data['product_price']);
                    }

                    if ($stts) {
                        $this->session->set_flashdata('flashMsg', 'Save data sucessfully.');
                        redirect('product');

                    }
                }
            }

        } else if (isset($action) && $action == 'edit') {
  
            $tableName = 'meter_tariff';
            $data['meterTariff'] = $this->Product_model->getRecord($tableName);
           
            $data['actionType'] = 'edit';
            $conditation = array('id' => $id);
            $TableNAme = 'product';
            $data['records'] = $this->Product_model->get_conditionData($conditation, $TableNAme);
            $oldProductPrice = $data['records']['product_price'];

            if ($this->input->post('save') == 'Save') {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('product_name', 'Product Name', 'trim|required');
                $this->form_validation->set_rules('partner_type', 'Partner Name', 'trim|required');
                $this->form_validation->set_rules('account_number', 'Account Number', 'required');
                $this->form_validation->set_rules('bank_name', 'Bank Name', 'required');
                $this->form_validation->set_rules('payment_occurrence', 'Payment Occurrence', 'required');

                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('flashMsg', 'Fill All the field.');
                    redirect('product');
                } else {

                    $dataupdate = array();
                    $dataupdate['product_name'] = $this->input->post('product_name');
                    $dataupdate['partner_type'] = $this->input->post('partner_type');
                    if (!empty($this->input->post('product_price'))) {
                        $dataupdate['product_price'] = $this->input->post('product_price');
                    } 
                    
                    $dataupdate['payment_type_id'] = $this->input->post('payment_occurrence');
                    $dataupdate['property_type'] = $this->input->post('property_type');
                    $dataupdate['product_type'] = $this->input->post('product_type');
                    $dataupdate['account_number'] = $this->input->post('account_number');
                    $dataupdate['bank_name'] = $this->input->post('bank_name');
                    $conditation = $id;
                    $TableNAme = 'product';
                    $data['records'] = $this->Product_model->updateRecord($TableNAme, $dataupdate, $conditation);

                    $data['type_of_property'] = $this->input->post('property_type');
                    $propertyType = $this->Product_model->getAllUsersWithPropertyType($data['type_of_property']); 
                    $count = count($propertyType);
                    $newProductPrice = (int)$dataupdate['product_price'];
                   
                    if($dataupdate['product_type'] != 'Power'){
                        for($i = 0; $i < $count; $i++){
                            $charge = $this->Product_model->getServiceCharge($propertyType[$i]->meter_no);
                          
                           
                            if((int)$charge[0]->amount_due <= 0){
                                if((int)$charge[0]->advance_payment > 0){
                                   // return true;
                                }else{
                                    $price = 0;
                                    $this->Product_model->editServiceCharge($charge[0]->meter_no, $price, $charge[0]->total_paid);
                                }
                            }else{
                                if((int)$charge[0]->amount_due >= $newProductPrice){
                                    if($newProductPrice == $oldProductPrice){
                                        //working
                                        $price =$newProductPrice - $oldProductPrice;
                                        $product_price = $newProductPrice - $price;
                                        $this->Product_model->editServiceCharge($charge[0]->meter_no, $product_price, $charge[0]->total_paid);
                                       
                                    }else if($newProductPrice > $oldProductPrice){
                                        //working
                                        $price = $newProductPrice - $oldProductPrice;
                                        $product_price = (int)$charge[0]->amount_due  + $price;
                                        $this->Product_model->editServiceCharge($charge[0]->meter_no, $product_price, $charge[0]->total_paid);
                                    }else{
                                        //working
                                        $price =  $oldProductPrice -$newProductPrice;
                                        $product_price = (int)$charge[0]->amount_due - $price;
                                        $this->Product_model->editServiceCharge($charge[0]->meter_no, $product_price, $charge[0]->total_paid);
                                    }
                                    
                                }else{
                                    if($newProductPrice >= $oldProductPrice){
                                        //working
                                        $price = $newProductPrice - $oldProductPrice;
                                        $product_price = (int)$charge[0]->amount_due  + $price;
                                        $this->Product_model->editServiceCharge($charge[0]->meter_no, $product_price, $charge[0]->total_paid);
                                    }else{
                                        $price =  $oldProductPrice - $newProductPrice;
                                        $product_price = $newProductPrice + $price;
                                    }
                                    // $product_price = (int)$charge[0]->amount_due + (int)$product_price;
                                    // $this->Product_model->editServiceCharge($charge[0]->user_id, $product_price, $charge[0]->total_paid);
                                }
                               
                            }
                            //$product_price = ($data['product_price'] / 100) * 1.4;
                            //$product_price = $data['product_price'] + $product_price + 100;
                        }
                    }             
                    $this->session->set_flashdata('success', 'Product Update Scuessfully');
                    redirect('product');
                }
            }

        } else if (isset($action) && $action == 'delete') {

            $tableName = 'product';
            $conditation = array('id' => $id);
            //$data = $this->Product_model->get_conditionData($conditation, $tableName);
            $value = $this->Product_model->getAmountDueByProId($id);
            $charge = $this->Product_model->getServiceChargeByProId($id);
            $product_price = (int)$charge[0]->product_price;
           
        
            $counter = count($value);
            for($i = 0; $i < $counter; $i++){
                $user_id = $value[$i]->user_id;
                $amount_due = (int)$value[$i]->amount_due;

                if($amount_due >= $product_price){
                    $amount_due = $amount_due -$product_price;
                    $this->Product_model->updateServiceChargeByProId($user_id, $id, $amount_due);
                }

            }

            //$this->Product_model->updateServiceAmountDue($conditation);
            if ($delete = $this->Product_model->delete($tableName, $conditation)) {
                $this->session->set_flashdata('success', 'Product Deleted Sucessfully');
                redirect('product');
            }
        }
        $this->template->load('default', 'product/admin/product_add', $data);

    }


    public function meter_tariff($action = NULL, $id = NULL)
    {

        $time = date('Y-m-d H:i:s');
        $data = array(
            'title' => 'Meter Tariff - Add'
        );
        if (isset($action) && $action == 'add') {
            $data['actionType'] = 'add';
            if ($this->input->post('save') == 'Save') {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('tariff_name', 'Tariff Name', 'trim|required');

                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('flashMsg', 'Fill All the field.');
                    redirect('product/meter_tariff/add/');
                } else {

                    $InsertDataArray = array();
                    $InsertDataArray['tariff_name'] = $this->input->post('tariff_name');
                    $InsertDataArray['tariff_price'] = $this->input->post('tariff_price');
                    $stts = $this->Product_model->setRecord('meter_tariff', $InsertDataArray);
                    if ($stts) {
                        $this->session->set_flashdata('flashMsg', 'Save data sucessfully.');
                        redirect('product/meter_tariff');

                    }
                }
            }
        } else if (isset($action) && $action == 'edit') {
            $data['actionType'] = 'edit';
            $conditation = array('id' => $id);
            $TableNAme = 'meter_tariff';
            $data['records'] = $this->Product_model->get_conditionData($conditation, $TableNAme);
            if ($this->input->post('save') == 'Save') {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('tariff_name', 'Full Name', 'trim|required');

                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('flashMsg', 'Fill All the field.');
                    redirect('product/meter_tariff/add/');
                } else {
                    $dataupdate = array();
                    $dataupdate['tariff_name'] = $this->input->post('tariff_name');
                    $dataupdate['tariff_price'] = $this->input->post('tariff_price');

                    $conditation = $id;
                    $TableNAme = 'meter_tariff';
                    $data['records'] = $this->Product_model->updateRecord($TableNAme, $dataupdate, $conditation);
                    //echo $this->bd>last_query();
                    $this->session->set_flashdata('success', 'Meter Tariff Update Scuessfully');
                    redirect('product/meter_tariff');
                }
            }

        } else if (isset($action) && $action == 'delete') {

            $tableName = 'meter_tariff';
            $conditation = array('id' => $id);
            if ($delete = $this->Product_model->delete($tableName, $conditation)) {
                $tableName = 'aauth_user_to_group';
                $conditation1 = array('user_id' => $id);
                if ($delete = $this->Product_model->delete($tableName, $conditation1)) {

                    $this->session->set_flashdata('success', 'Meter Tariff Deleted Sucessfully');
                    redirect('product/meter_tariff');

                }

            }


        } else {
            $tableName = 'meter_tariff';
            $data['records'] = $this->Product_model->getRecord($tableName);
        }

        $this->template->load('default', 'product/admin/meter_tariff', $data);

    }

    public function userguide($action = NULL, $id = NULL){
        $this->template->load('default', 'users/admin/userguide');
    }

    public function announcement($action = NULL, $id = NULL)
    {

        $time = date('Y-m-d H:i:s');
        $data = array(
            'title' => 'Announcement - Add'
        );
        if (isset($action) && $action == 'add') {
            $data['actionType'] = 'add';
            if ($this->input->post('save') == 'Save') {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('tittle', 'Tittle', 'trim|required');
                $this->form_validation->set_rules('description', 'Description Name', 'trim|required');

                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('flashMsg', 'Fill All the field.');
                    redirect('product/announcement/add/');
                } else {

                    $InsertDataArray = array();
                    $InsertDataArray['tittle'] = $this->input->post('tittle');
                    $InsertDataArray['description'] = $this->input->post('description');
                    $InsertDataArray['created_date'] = $time;
                    $stts = $this->Product_model->setRecord('announcement', $InsertDataArray);
                    if ($stts) {
                        $this->session->set_flashdata('flashMsg', 'Save data sucessfully.');
                        redirect('product/announcement');

                    }
                }
            }
        } else if (isset($action) && $action == 'edit') {
            $data['actionType'] = 'edit';
            $conditation = array('id' => $id);
            $TableNAme = 'announcement';
            $data['records'] = $this->Product_model->get_conditionData($conditation, $TableNAme);
            if ($this->input->post('save') == 'Save') {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('tittle', 'Tittle', 'trim|required');
                $this->form_validation->set_rules('description', 'Description Name', 'trim|required');

                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('flashMsg', 'Fill All the field.');
                    redirect('product/announcement/add/');
                } else {
                    $dataupdate = array();
                    $dataupdate['tittle'] = $this->input->post('tittle');
                    $dataupdate['description'] = $this->input->post('description');

                    $conditation = $id;
                    $TableNAme = 'announcement';
                    $data['records'] = $this->Product_model->updateRecord($TableNAme, $dataupdate, $conditation);
                    //echo $this->bd>last_query();
                    $this->session->set_flashdata('success', 'Meter Tariff Update Scuessfully');
                    redirect('product/announcement');
                }
            }

        } else if (isset($action) && $action == 'delete') {

            $tableName = 'announcement';
            $conditation = array('id' => $id);
            if ($delete = $this->Product_model->delete($tableName, $conditation)) {

                $this->session->set_flashdata('success', 'Announcement Deleted Sucessfully');
                redirect('product/announcement');


            }


        } else {
            $tableName = 'announcement';
            $data['records'] = $this->Product_model->getRecord($tableName);
        }

        $this->template->load('default', 'product/admin/announcement', $data);

    }


}
