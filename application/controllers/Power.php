<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Power extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('Power_model');
        $this->load->model('Product_model');
        $this->load->model('User_model');
        $this->load->model('Payment_model');
        if (!$this->aauth->is_loggedin())
            return redirect('login', 'refresh'); 
        if (!$this->aauth->is_member('Admin')){
            show_error('Access Denied');
        }
    }

    public function index(){

        $data = array(
            'title' => 'Vend Power'
        );

        $data['payments'] =  $this->Payment_model->getAdminPayment();
        $this->template->load('default', 'powers/index', $data);
    }

    function GenerateOrder($length)
    {
        $code = "";
        $possible = "012346789";
        $maxlength = strlen($possible);
        if ($length > $maxlength) {
            $length = $maxlength;
        }
        $i = 0;
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, $maxlength - 1), 1);
            if (!strstr($code, $char)) {
                $code .= $char;
                $i++;
            }
        }
        return $code;
    }

    public function create(){
        $data = array(
            'title' => 'Generate Power'
        );
        $data['order_id'] = "55" . $this->GenerateOrder(10);
        $data['narration'] = 'Admin Power Vending';
        $data['users'] = $this->User_model->user_type_record('Public');

        $this->template->load('default', 'powers/create',$data);
    }

    public function saveTransaction(){
        $user_id = $this->input->post('user', true);
        $amount = $this->input->post('amount', true);
        $charge = $this->input->post('charge', true);
        $payment_id = $this->input->post('order_id', true);

        $userInfo = $this->db->query("SELECT * FROM aauth_users WHERE id = '$user_id'")->result();
        $meter_no = $userInfo[0]->meter_no;
        $email = $userInfo[0]->email;
        $full_name = $userInfo[0]->full_name;

        $_SESSION['payment_id'] = $payment_id;
        $_SESSION['charge'] = $charge;
        $_SESSION['amount'] = $amount;
        $_SESSION['meter_no'] = $meter_no;
        $_SESSION['email'] = $email;
        $_SESSION['user_id'] = $user_id;
        $_SESSION['full_name'] = $full_name;
        $_SESSION['type'] = 'Power';
        $_SESSION['status'] = 'Pending';
        $_SESSION['payment_type'] = 'Card';

         // Power charge - the estate monthly charge (5000)
         $_SESSION['token_amount'] = (int) $_SESSION['amount']   - (int)$_SESSION['charge'];

         //Save transaction into the payment table
         $data = array(
            'user_id' => $_SESSION['user_id'], 
            'amount' =>  $_SESSION['amount'], 
            'payment_id' => $_SESSION['payment_id'], 
            'meter_no' => $_SESSION['meter_no'],
            'type' => $_SESSION['type'],
            'status' =>  $_SESSION['status'],
            'payment_type' => $_SESSION['payment_type'],
            'admin_payment' => 1
        );

        //insert pending transaction into the database
        if($this->Payment_model->save($data)){
            echo json_encode(array('code'=>'00'));
        }
    }

    private function send_payment_email($Maildata)
    {
        $to = $Maildata['to'];
        $subject = 'Payment Completed successful';
        $config = array(
            'mailtype' => 'html',
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_user' => 'saddle@netplusadvisory.com',
            'smtp_pass' => 'Saddle7890',
            'smtp_port' => 465,
            'priority' => 1,
            'newline' => "\r\n"
        );
        $message = $this->load->view('shopping/pay-success.php', $Maildata, true);
        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('saddle@netplusadvisory.com', 'Richmond Gate Phase 1 Estate Association');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $r = $this->email->send();
    }

    public function verify_meter(){
        $user_id = $this->input->get('user', true);
        $userInfo = $this->db->query("SELECT * FROM aauth_users WHERE id = '$user_id'")->result();
        $meter_no = $userInfo[0]->meter_no;

        $code = $this->verifyVending($meter_no);
        if($code == '00'){
            echo json_encode(array('code'=>'00'));
        }else{
            echo json_encode(array('code'=>'90'));
            $this->session->set_flashdata('errors', 'Meter not could not be verified. Try again later');
        }
    }

    private function verifyVending($MeterNo)
    {
       
        // $endpoint = "http://41.216.166.165:8080/MEMMCOLWebServices/webresources/IdentificationV2/102/$MeterNo";

        // $session = curl_init($endpoint);  
        // curl_setopt($session, CURLOPT_HEADER, false);
        // curl_setopt($session, CURLOPT_RETURNTRANSFER, true);// return values as a string, not to std out
        
        // $responsexml = curl_exec($session);                     // send the request
        // $httpcode = curl_getinfo($session, CURLINFO_HTTP_CODE);
        // $xml = simplexml_load_string($responsexml);
        // $responsexml = json_encode($xml);

        // curl_close($session);
        // if ($httpcode == 200) {
        //     $tokenDecode = json_decode($responsexml);   
        //     $responseCode = $tokenDecode->responsecode;
        //     return $responseCode;
        // }
       // return false;
        return '90';
    }

    public function generatePowerToken()
    {
        $user_id = $_SESSION['user_id'];
        $amount =  $_SESSION['amount'];
        $charge =  $_SESSION['charge'];
        $payment_id = $_SESSION['payment_id'];
        $MeterNo =  $_SESSION['meter_no'];

        
        $payment_id = $payment_id;
        $Tokenamount =  $_SESSION['token_amount'];
        $vend = $this->vend($payment_id, $MeterNo, $Tokenamount);

        
        $this->Product_model->update(['vend_log'=>print_r($vend)],$payment_id);

        if ($vend) {
            return $vend;
        }
        
        return false;
    }

    public function vend($payment_id, $MeterNo, $Tokenamount){
        // $url = "http://41.216.166.165:8080/MEMMCOLWebServices_Pilot/webresources/PaymentV2/$MeterNo/prepaid/102/$payment_id/$Tokenamount";
        // $url = "http://41.216.166.165:8080/MEMMCOLWebServices/webresources/PaymentV2/$MeterNo/prepaid/102/$payment_id/$Tokenamount";
        //  $ch = curl_init($url);
        //  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
        //  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //  curl_setopt($ch, CURLOPT_HEADER, false);
        //  $result = curl_exec($ch);
 
        //  if($result == false)
        //  {
        //      echo json_encode([curl_error($ch)]);
        //  }
        //  $xml = simplexml_load_string($result);
        //  $result = json_encode($xml);
 
         
        //  //$res = json_decode($result);
        //  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
 
        //  curl_close($ch); 
        //  if ($httpcode == 200) {
        //      $tokenDecode = json_decode($result);   
        //      $responseCode = $tokenDecode->responsecode;
    
        //      return $tokenDecode;
        //  }
         return false;
 
        // $dataToken = json_encode(array(
        //         'recieptNumber' => '4859 9945 9546',
        //         'responsecode' => '00', 
        //         'responsedesc' => '095869495',
        //         'token_amount' => '50',
        //         'paidamount' =>  $Tokenamount,
        //         'costOfUnit' => '500',
        //         'unit' => '10',
        //         'vat' => '60'
        //     )
        // );
        // $tokenDecode = json_decode($dataToken);
        // return $tokenDecode;
    }

    

    public function payment_response()
    {
        //Current user data
        $userId = $_SESSION['user_id'];
        $MeterNo = $_SESSION['meter_no'];
        $charge = $_SESSION['charge'];

        $code = $this->input->get('code', true);
        $trans_id = $this->input->get('trans_id', true);
        $order_id = $this->input->get('orderid', true);
        $amount = $this->input->get('amount', true);
        $result = $this->input->get('result', true);
        $message = $this->input->get('message', true);
        $bank = $this->input->get('bank', true);
        $order_id = str_replace(' ', '', $order_id);

        $Maildata = array();
        $Maildata['full_name'] = $_SESSION['full_name'];
        $Maildata['to'] =$_SESSION['email'];
        $Maildata['order_id'] = $order_id;
        $Maildata['trans_ref'] = $trans_id;
        $Maildata['bank'] = $bank;
        $Maildata['amount_paid'] = $amount;
        $Maildata['reason'] = $message;
        $Maildata['tr_date'] = date('d/m/Y');
        $Maildata['tr_time'] = date('H:i');
        $Maildata['token_no'] = '';
        $Maildata['token_desc'] = '';
        $Maildata['token_amount'] = '';
        $Maildata['is_power'] = 1;
        $Maildata['product_name'] = 'Power';
     
        if (empty($code) || empty($trans_id) || empty($order_id) || empty($result) || empty($message)) {
            $error = 'Missing parameters.';
        } else {
            $data = array(
                'transaction_id' => $trans_id,
                'bank' => $bank,
                'amount_paid' => $amount,
                'payment_description' => $message,
                'payment_id' => $order_id
            );
            $payment_verify = $this->Payment_model->get(array('payment_id' => $order_id));
            if ($payment_verify) {
                if ($payment_verify && $payment_verify->status == 'Pending') {
                    if ($code == '00') {
                        $r_ver_data = ['order_id' => $order_id, 'trans_id' => $trans_id];

                      //  if ($this->verifyNetplusPayment($payment_verify, $r_ver_data)) {

                            $data['status'] = 'Paid';
                            $Maildata['payment_status'] = 'Paid';
                            $this->Product_model->update($data, $payment_verify->id);
                       
                            $amount = (int)$amount - (int)$charge;

                         
                            $Maildata['product_name'] = 'Power';
            
                            $Maildata['is_power'] = 1;
                            $Tokenamount = $amount;
                    
                            $xmlrequest = array('transactionId' => $order_id, 'meterNumber' => $MeterNo, 'amount' => $Tokenamount);
                            $powerToken = $this->generatePowerToken($Tokenamount, $order_id);
                     
                            
                            //"Vendin api call after send token';
                            $this->Product_model->update(['vend_log'=>json_encode($powerToken)], $payment_verify->id);
                            
                            if ($powerToken) {
                                // Change this to 00 before live
                                if ($powerToken->responsecode == '00') {
                                    
                                    $Maildata['token_no'] = $powerToken->recieptNumber;
                                    $dataToken = array('token_no' => $powerToken->recieptNumber, 'token_desc' => $powerToken->responsedesc, 'token_amount' => $powerToken->unit);
                                    

                                    $this->Product_model->update(['vend_log'=>json_encode($dataToken)], $payment_verify->id);
                                    $this->Product_model->update($dataToken, $payment_verify->id);

                                } else {

                                    // $dataToken = array(
                                    //         'resp_code' => $powerToken->responsecode, 
                                    //         'token_desc' => $powerToken->responsedesc,
                                    //         'token_amount' => $powerToken->unit,
                                    //         'paidamount' => $powerToken->paidamount,
                                    //         'costOfUnit' => $powerToken->costOfUnit,
                                    //         'unit' => $powerToken->unit,
                                    //         'vat' => $powerToken->vat
                                    //         );
                                    // $dataToken = array('token_no' => $powerToken->recieptNumber, 'token_desc' => $powerToken->responsedesc, 'token_amount' => $powerToken->unit);
                                    $this->Product_model->update(['vend_log'=>json_encode($powerToken)], $payment_verify->id);
                                    // $this->Product_model->update($dataToken, $payment_verify->id);
                                }
                                $Maildata['token_desc'] = $powerToken->responsedesc;
                                $Maildata['token_amount'] = $powerToken->unit;
                            }
                          
                    
                            
                            $this->session->set_flashdata('success', 'Payment Completed successful.');

                            //Send payment email
                            $this->send_payment_email($Maildata);

                            unset($_SESSION['order_id']);
                            unset($_SESSION['cart_session']);

                            redirect('power/pay_success/' . $order_id);
                            exit;
                        // } else {
                        //     $error = 'Payment verification failed.';
                        // }
                    }
                    if ($code !== '00') {
                        $data['status'] = 'Payment Failed';
                        $error = $message;
                    }
                } else {
                    $error = 'Order payment status ('.$payment_verify->status.') invalid.';
                }
                $data['payment_description'] = $error;
                $this->Product_model->update($data, $payment_verify->id);
            }
        }
        unset($_SESSION['order_id']);
        unset($_SESSION['cart_session']);
        redirect('power/pay_failed/' . $order_id);
    }

    function pay_success($orderId)
    {
        $data['is_power'] = 1;
        $data['product_name'] = 'Power';
        
        $OrderDetails = $this->db->query("select * from payments where   payment_id ='" . $orderId . "'")->row();

        $data['full_name'] = $_SESSION['full_name'];
        $data['tr_date'] = date('Y-M-d', strtotime($OrderDetails->date_created));
        $data['tr_time'] = date('H:i', strtotime($OrderDetails->date_created));;
        $data['order_id'] = $orderId;
        $data['amount_paid'] = $OrderDetails->amount_paid;
        $data['token_no'] = $OrderDetails->token_no;
        $data['token_desc'] = $OrderDetails->token_desc;
        $data['token_amount'] = $OrderDetails->token_amount;

        $data['title'] = 'Payment successful';
        unset($_SESSION['payment_id']);
        unset($_SESSION['amount']);
        unset($_SESSION['meter_no']);
        unset($_SESSION['email']);
        unset($_SESSION['charge']);
        unset($_SESSION['user_id']);
        unset($_SESSION['full_name']);
        unset($_SESSION['type']);
        unset($_SESSION['status']);
        unset($_SESSION['payment_type']);
        unset($_SESSION['order_id']);
        $this->template->load('default', 'shopping/pay-success', $data);

    }

    function pay_failed($orderId)
    {
        $OrderDetails = $this->db->query("select * from payments where   payment_id ='" . $orderId . "'")->row();
        if ($OrderDetails) {
     
            $data['is_power'] = 1;
            $data['product_name'] = 'Power';
            $data['full_name'] = $_SESSION['full_name'];
            $data['tr_date'] = date('Y-M-d', strtotime($OrderDetails->date_created));
            $data['tr_time'] = date('H:i', strtotime($OrderDetails->date_created));;
            $data['order_id'] = $orderId;
            $data['amount_paid'] = $OrderDetails->amount_paid;
            $data['token_no'] = $OrderDetails->token_no;
            $data['token_desc'] = $OrderDetails->token_desc;
            $data['token_amount'] = $OrderDetails->token_amount;
            $data['reason'] = $OrderDetails->payment_description;
        }


        $data['title'] = 'Payment Failed';
        unset($_SESSION['payment_id']);
        unset($_SESSION['amount']);
        unset($_SESSION['meter_no']);
        unset($_SESSION['email']);
        unset($_SESSION['charge']);
        unset($_SESSION['user_id']);
        unset($_SESSION['full_name']);
        unset($_SESSION['type']);
        unset($_SESSION['status']);
        unset($_SESSION['payment_type']);
        unset($_SESSION['order_id']);
        $this->template->load('default', 'shopping/pay-failed', $data);

    }

}