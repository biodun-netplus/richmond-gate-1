<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if ($this->aauth->is_loggedin())
            redirect('dashboard', 'refresh');
	}
    
    public function index(){
	
		

        $data = array(
            'title' => 'Login'
        );
        $this->template->load('login','login/login',$data);
    }

    public function register2(){

        $data = array(
            'title' => 'Register'
        );
        $this->template->load('login','login/register',$data);
    }

    public function forgotpassword(){
        $data = array(
            'title' => 'Forgot Password'
        );
        $this->template->load('login', 'login/forgotpassword', $data);
    }

    public function forgotpass(){
      
        $email = $this->input->post('email');
        $query = $this->db->query("SELECT * FROM `aauth_users` WHERE `email` = '$email'");
        if($query->num_rows() > 0){
            $user = $query->result();
            $pass = substr(str_shuffle(str_repeat("23456789abcdefghijklmnopqrstuvwxyz", 8)), 0, 8);
            $userid = $user[0]->id;
            $fullname = $user[0]->full_name;
            $email = $user[0]->email;
            
           
            $password = $this->aauth->hash_password($pass, $userid);
            $this->db->query("UPDATE `aauth_users` SET `pass` = '$password' WHERE `id` = '$userid'");

            $to = $email;
            $subject = 'Password Reset Confirmation and Login Details';
            
            $config = array(
                'mailtype' => 'html',
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_user' => 'saddle@netplusadvisory.com',
                'smtp_pass' => 'Saddle7890',
                'smtp_port' => 465,
                'priority' => 1,
                'newline' => "\r\n"
            );

            $data = array(
                'fullname' => $fullname,
                'email' => $email,
                'password' => $pass
            );

            $message = $this->load->view('email/password_reset.php', $data, true);

            $this->load->library('email', $config);
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from('saddle@netplusadvisory.com', 'Richmond Gate Phase 1 Estate Association');
            $this->email->to($to);

            $this->email->subject($subject);
            $this->email->message($message);

            $r = $this->email->send();
          

            $this->session->set_flashdata('success', 'Password reset successfully, Check your email.');
            return redirect('/', 'refresh');
        }else{
            $this->session->set_flashdata('errors', 'Account not found, please contact Administator.');
            redirect('login/forgotpassword', 'refresh');
        }
        return redirect('/', 'refresh');
    }
    public function auth(){

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        
        if ($this->form_validation->run() == TRUE)
        {
            if ($this->aauth->login($this->input->post('email',true),$this->input->post('password',true)))
                redirect('dashboard', 'refresh');
            
        }
        
        $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors'))); 
        
        $this->session->set_flashdata('errors', $errors);
        //exit();
        redirect('login', 'refresh');

        
        
    }

    public function register(){
      
        $data = array(
            'title' => 'Register'
        );
        if ($this->input->server('REQUEST_METHOD') == 'POST'){
            $this->form_validation->set_rules('firstname', 'First Name', 'required');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required');
            $this->form_validation->set_rules('meter_no', 'Meter No', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('house_address', 'House Address', 'required');
            $this->form_validation->set_rules('type_of_ownership', 'Ownership Type', 'required');
           // $this->form_validation->set_rules('cug_no', 'CUG Number', 'required');
            $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('confirm_password', 'Password', 'required|matches[password]');


            if ($this->form_validation->run() == TRUE)
            {
                $firstname = $this->input->post('firstname',true);
                $lastname = $this->input->post('lastname',true);

                $fullname = $firstname . ' ' . $lastname;

                $data = array(
                    'fullname' => $fullname,
                    'email' => $this->input->post('email',true),
                    'password' => $this->input->post('password',true)
                );
                $meter_no = $this->input->post('meter_no');

                //$house_address = $this->input->post('house_address');
                //$house_number = trim($house_address);
                //$house_number = str_replace(' ', '', $house_number);
                //$house_num = $this->db->query("SELECT * FROM `house_address` WHERE `house_number` LIKE '%$house_number%'");
                //if($house_num->num_rows() > 0){
                    // $meter_no = $this->input->post('meter_no');

                    // if(isset($meter_no)){
                    //     $meter_no = rand(1111111111,9999999999);
                    // }

                    $check = $this->db->query("SELECT * FROM `aauth_users` WHERE `meter_no` = '$meter_no'");
                    if($check->num_rows() > 0 && $check->num_rows() < 2){
                        if($userid = $this->aauth->create_user($data['email'], $data['password'])){
                            if($this->aauth->add_member($userid,'Public')){
                                $additional_data =  array(
                                    'full_name' => $fullname,
                                    'meter_no' => $meter_no,
                                    'house_address' => $this->input->post('house_address',true),
                                    'type_of_ownership' => $this->input->post('type_of_ownership', true),
                                    'type_of_property' => $this->input->post('type_of_property',true),
                                    'mobile_no' => $this->input->post('mobile_no',true),
                                    'cug_no' => $this->input->post('cug_no',true),
                                    'partner_type' => 'Partner'
                                );
                                $to = $data['email'];
                                $subject = 'Registration Confirmation and Login Details';
                                
                                $config = array(
                                    'mailtype' => 'html',
                                    'protocol' => 'smtp',
                                    'smtp_host' => 'ssl://smtp.gmail.com',
                                    'smtp_user' => 'saddle@netplusadvisory.com',
                                    'smtp_pass' => 'Saddle7890',
                                    'smtp_port' => 465,
                                    'priority' => 1,
                                    'newline' => "\r\n"
                                );
        
        
                                $message = $this->load->view('email/registration_mail.php', $data, true);
        
                                $this->load->library('email', $config);
                                $this->email->initialize($config);
                                $this->email->set_newline("\r\n");
                                $this->email->from('saddle@netplusadvisory.com', 'Richmond Gate 1 Estate Association');
                                $this->email->to($to);
        
                                $this->email->subject($subject);
                                $this->email->message($message);
        
                                $r = $this->email->send();
        
                                    
                                $this->aauth->aauth_db->where('id', $userid);
                                $this->aauth->aauth_db->update('aauth_users', $additional_data);
        
                            
                                $this->session->set_flashdata('success', 'Account created successfully, please login.');
                                return redirect('/', 'refresh');
                            
    
                            }
                        }
                       // $this->session->set_flashdata('errors', 'Meter number already exist');
                       // redirect('/registration', 'refresh');
                    }elseif($check->num_rows() >= 2 ){
                        $this->session->set_flashdata('errors', "You can only register 2 users per room");
                        redirect('/registration', 'refresh');
                    }else{
                        
                        if($userid = $this->aauth->create_user($data['email'], $data['password'])){
                            if($this->aauth->add_member($userid,'Public')){
                                $additional_data =  array(
                                    'full_name' => $fullname,
                                    'meter_no' => $meter_no,
                                    'house_address' => $this->input->post('house_address',true),
                                    'type_of_ownership' => $this->input->post('type_of_ownership', true),
                                    'type_of_property' => $this->input->post('type_of_property',true),
                                    'mobile_no' => $this->input->post('mobile_no',true),
                                    'cug_no' => $this->input->post('cug_no',true),
                                    'partner_type' => 'Partner'
                                );
                                $to = $data['email'];
                                $subject = 'Registration Confirmation and Login Details';
                                
                                $config = array(
                                    'mailtype' => 'html',
                                    'protocol' => 'smtp',
                                    'smtp_host' => 'ssl://smtp.gmail.com',
                                    'smtp_user' => 'saddle@netplusadvisory.com',
                                    'smtp_pass' => 'Saddle7890',
                                    'smtp_port' => 465,
                                    'priority' => 1,
                                    'newline' => "\r\n"
                                );
        
        
                                $message = $this->load->view('email/registration_mail.php', $data, true);
        
                                $this->load->library('email', $config);
                                $this->email->initialize($config);
                                $this->email->set_newline("\r\n");
                                $this->email->from('saddle@netplusadvisory.com', 'Richmond Gate 1 Estate Association');
                                $this->email->to($to);
        
                                $this->email->subject($subject);
                                $this->email->message($message);
        
                                $r = $this->email->send();
        
                                    
                                $this->aauth->aauth_db->where('id', $userid);
                                $this->aauth->aauth_db->update('aauth_users', $additional_data);
        
        
                                $type_of_property = $additional_data['type_of_property'];
                                $file = $this->db->query("SELECT * FROM `product` WHERE `property_type` = '$type_of_property' and `product_type` = 'Service'");
                                if($file->num_rows() > 0){
                                    $product = $file->result();
                                
                                    $meter_no = $additional_data['meter_no'];
                                    $product_id = $product[0]->id;
                                    $product_price = $product[0]->product_price;
                                    $this->db->query("INSERT INTO `service_charges` (`meter_no`, `user_id`, `product_id`, `amount_due`) VALUES ('$meter_no', '$userid', '$product_id', '$product_price') ");    
                                }
                               
                                $this->session->set_flashdata('success', 'Account created successfully, please login.');
                                return redirect('/', 'refresh');
                            
    
                            }
                        }
                    } 
                // }else{
                //     $this->session->set_flashdata('errors', 'House Address does not exist');
                //     redirect('/registration', 'refresh');       
                // }
            }

            $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors')));
            $this->session->set_flashdata('errors', $errors);
        }
        $this->template->load('login','login/register',$data);
    }

}