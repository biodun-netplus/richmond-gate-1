<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['404_override'] = 'Error/index';
$route['translate_uri_dashes'] = FALSE;


$route['registration'] = 'login/register';
$route['profile/settings'] = 'dashboard/profile';
$route['profile/setting/update'] = 'dashboard/profile_update';
$route['profile/setting/change-password'] = 'dashboard/change_password';
$route['profile/paymentsettings'] = 'dashboard/payment_settings';
$route['profile/paymentsettings/payment-occurrence'] = 'dashboard/payment_occurrence';


$route['payment/list'] = 'payment/all';


$route['admin/user/new'] = 'admin/user_new';
$route['admin/user/create'] = 'admin/user_create';
$route['admin/user/edit/(:num)'] = 'admin/user_edit/$1';
$route['admin/user/update/(:num)'] = 'admin/user_update/$1';
$route['admin/user/delete/(:num)'] = 'admin/user_delete/$1';


$route['admin/merchant/new'] = 'admin/merchant_new';
$route['admin/merchant/create'] = 'admin/merchant_create';
$route['admin/merchant/edit/(:num)'] = 'admin/merchant_edit/$1';
$route['admin/merchant/update/(:num)'] = 'admin/merchant_update/$1';
$route['admin/merchant/delete/(:num)'] = 'admin/merchant_delete/$1';
$route['admin/form/component/saddle'] = 'admin/saddle_form';



$route['order/new'] = 'order/order_new';
$route['order/create'] = 'order/order_create';
$route['order/list'] = 'order/order_list';
$route['order/list/(:any)'] = 'order/order_list';
$route['order/form/component/delivery'] = 'order/delivery_form';
$route['order/update/customer/(:any)'] = 'order/update_customer/$1';
$route['order/update/delivery/(:any)'] = 'order/update_delivery/$1';


$route['report/order'] = 'order/order_report';
$route['report/order/(:any)'] = 'order/order_report';




