<div class="container" id="registration-form">
    <div class="row">
        <div class="col-md-7 col-xs-12 hidden-xs login-heading">
            <div>
                <p>Welcome to</p>
            </div>
            <div>
                <p style="background: #071c0e;">Richmond Gate Phase 1 Estate residents and</p>
            </div>
            <div>
                <p style="background:#324a17;">utilities management portal</p>
            </div>
        </div>
        <div class="col-md-5 col-xs-12">
            <div align="center">
                <img src="https://netpluspay.com/images/netpluspayLogoGreen.png" class="login-logo" style="height: 50px; width: 200px;"/>
                <br/>
            </div>            <?php $this->load->view('includes/notification'); ?>
            <div class="panel login-panel">
           
                <form id="validate-form" class="form-horizontal"  method="post" data-parsley-validate="" "autocomplete"="off" accept-charset="utf-8">


                <div class="panel-body">
                <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" name="firstname" id="FirsttName"
                                   placeholder="First Name" required value="<?php echo set_value('firstname'); ?>" >
                        </div>

                    </div>

                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" name="lastname" id="LasttName"
                                   placeholder="Last Name" required value="<?php echo set_value('lastname'); ?>" >
                        </div>

                    </div>
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" name="meter_no" id="metterno"
                                   placeholder="Meter Number"   value="<?php echo set_value('meter_no'); ?>">
                        </div>
                    </div>
                    
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" name="mobile_no" id="mobile_no"
                                   placeholder="Mobile Number" required value="<?php echo set_value('mobile_no'); ?>" >
                        </div>
                    </div>
                    
                     <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" name="cug_no" id="cug_no"
                                   placeholder="CUG Number"  value="<?php echo set_value('cug_no'); ?>" >
                        </div>
                    </div>
                    
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                           <select class="form-control" name="type_of_ownership" required >
                                <option>Ownership Type</option>
                                <option value="landlord">Landlord</option>
                                <option value="resident">Resident</option>
                            </select>
                        </div>
                    </div>
                    
                    
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" name="house_address" id="house_address"
                                   placeholder="House Address" required value="<?php echo set_value('house_address'); ?>" >
                        </div>
                    </div>
                    
                    
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                           <select class="form-control" name="type_of_property" required >
                                <option>Select Property</option>
                                <option value="Semi Detached">Semi Detached</option>
                                <option value="Terrace">Terrace</option>
                                <option value="Flat">Flats (3-BR)</option>
                                <option value="Maisonettes">Maisonettes (4-BR)</option>
                                <option value="Quads">Quads (4- & 5-BR)</option>
                                <option value="Townhouses">Townhouses (4-BR)</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" required data-parsley-type="email"  name="email" id="Email" placeholder="Email" value="<?php echo set_value('email'); ?>">
                        </div>
                    </div>
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="password" class="form-control" name="password" id="Password"
                                   placeholder="Password" data-parsley-minlength="8"  required>
                        </div>
                    </div>
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="password" class="form-control" name="confirm_password" id="ConfirmPassword"
                                   placeholder="Confirm Password" data-parsley-minlength="8"  data-parsley-equalto="#Password" required>
                        </div>
                    </div>
                    <!-- <div style="max-height: 150px;overflow-y: scroll;background:white;padding:10px;"><h5 style="text-align: justify;"><strong>TERMS &amp; CONDITIONS OF USE OF THE ELECTRONIC UTILITIES PORTAL&nbsp;</strong></h5>
<p style="text-align: justify;">By enrolling on the portal, I accept the under listed Terms &amp; Conditions (as may be amended from time to time) and covenant to abide by these terms.&nbsp;</p>
<ol style="text-align: justify;">
<li>I shall dutifully pay for my service Charge at the rates stipulated below or such other rates as may be stipulated by the Residents Association, from time to time, failing which I shall be liable to immediate disconnection from estate amenities in addition to liability to pay for any outstandings:
<ol>
<li><strong>Metro:&nbsp;</strong>N48,750.00 per annum</li>
<li><strong>Terrace: </strong>N195,000.00 per annum</li>
<li><strong>Semi-Detached duplex: </strong>N215,000.00 per annum.</li>
<li><strong>Private Area: </strong>N300,000.00 per annum&nbsp;</li>
</ol>
</li>
</ol>
<p style="text-align: justify;">I shall dutifully pay for the applicable power charges (as at when due) as follows:&nbsp;</p>
<ol style="text-align: justify;">
<li><strong>Metro:</strong>
<ul>
<li><strong>Base Charge</strong> - N2,500.00 (Partner),&nbsp; N9,000.00 (Non Partner), and&nbsp;<strong>Take or Pay</strong> - N8,000.00 per month</li>
</ul>
</li>
<li><strong>Terrace or Semi-Detached duplex:</strong>
<ul>
<li><strong>Base Charge</strong> - N4,000.00 (Partner), N18,000.00 (Non Partner), and <strong>Take or Pay&nbsp;</strong>- N20,000.00 per month</li>
</ul>
</li>
<li>I shall dutifully pay for the applicable CUG fee of N500.00 per month or any other applicable amount</li>
<li>I shall be liable to pay a fine of N250,000.00 (in addition to monthly liability for Base Charge &amp; Take or Pay for the applicable period) for any bypassing of the electricity system or my meter and I shall ensure that no tampering or repairs of my meter is carried out without authorization from the Association as well as the metering company.</li>
<li>To pay my generator levy of N300,000.00.</li>
<li>I consent to pay the applicable charges for transactions done on the platform as follows:
<ol>
<li>1.4% or any other applicable rate of the amount of payment made as transaction fees</li>
<li>N100.00 convenience fee for every transaction&nbsp;</li>
</ol>
</li>
</ol>
<ol style="text-align: justify;" start="7">
<li>In the event of damage to my meter or purchase of a new meter, to pay N90,000.00 or any applicable amount to the Association as cost for a new or replacement meter and allow at least 3 weeks after payment before the deployment of the meter.</li>
<li>In the event that I will be unavailable in the estate for a period longer than a month, to formally notify the Association of my absence for purposes of discounting my Take or Pay obligations only, whilst I will ensure prompt payment of all other obligations (Service charge, base charge, CUG etc).</li>
<li>Not to engage in any actions calculated at undermining my financial obligations to the Association.</li>
<li>In the event that I want to relocate out of the estate, to ensure immediate settlement of all my financial obligations and where I fail to, I shall remain liable for all outstanding payments.</li>
<li>In the event of any complaint regarding my use of the Utilities Portal such as erroneous token or wrongful debit of my account, to escalate to the representatives of association for resolution.</li>
</ol>
<p style="text-align: justify;">&nbsp;For further clarification, please contact the estate office.</p>
<p style="text-align: justify;">&nbsp;I covenant to be bound by and abide with the foregoing Terms of Use of the Estate Utilities Portal.</p>
</div> -->
                    <div class="form-group mb-n">
                        <div class="col-xs-offset-2 col-xs-8">
                            <div class="checkbox checkbox-inline checkbox-primary">
                                <label>
                                    <input type="checkbox" name="terms"  required />&nbsp;&nbsp;I accept the user
                                        agreement
                                </label>
                            </div>

                        </div>
                    </div>


                </div>
                <div class="panel-footer">
                    <div class="clearfix">
                        <button type="submit" class="btn btn-login btn-raised pull-left">Register</button>
                        <a href="<?php echo site_url('/'); ?>" class="btn btn-default pull-right">Already Registered?
                            Login</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
