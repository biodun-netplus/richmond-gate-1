<!DOCTYPE html>
<html>

<head>
   <title><?php echo $title; ?> - Richmond</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    

    <link rel="shortcut icon" href="http://willcoonline.com.ng/richmond-gate1/assets/assets/img/logo-icon-dark.png">

    <link type='text/css' href='http://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500' rel='stylesheet'>
    <link type='text/css' href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/progress-skylo/skylo.css" type="text/css" rel="stylesheet">

    <link href="http://willcoonline.com.ng/richmond-gate1/assets/assets/fonts/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="http://willcoonline.com.ng/richmond-gate1/assets/assets/css/styles.css" type="text/css" rel="stylesheet">
    <link href="http://willcoonline.com.ng/richmond-gate1/assets/assets/css/custom.css" type="text/css" rel="stylesheet">

    
</head>

<body class="focused-form animated-content login-custom" style="background:url('http://willcoonline.com.ng/richmond-gate1/assets/assets/img/Bg-image.jpg');background-size:cover;">

<?=$body?>


     <!-- Load page level scripts-->

<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/js/jquery-1.10.2.min.js"></script> 							<!-- Load jQuery -->
<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/js/jqueryui-1.10.3.min.js"></script> 							<!-- Load jQueryUI -->
<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/js/bootstrap.min.js"></script> 								<!-- Load Bootstrap -->
<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/js/enquire.min.js"></script> 									<!-- Load Enquire -->

<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/velocityjs/velocity.min.js"></script>					<!-- Load Velocity for Animated Content -->
<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/velocityjs/velocity.ui.min.js"></script>

<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/progress-skylo/skylo.js"></script> 		<!-- Skylo -->

<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/wijets/wijets.js"></script>     						<!-- Wijet -->

<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/sparklines/jquery.sparklines.min.js"></script> 			 <!-- Sparkline -->

<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/codeprettifier/prettify.js"></script> 				<!-- Code Prettifier  -->

<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>  <!-- Bootstrap Tabdrop -->

<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->

<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/dropdown.js/jquery.dropdown.js"></script> <!-- Fancy Dropdowns -->
<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/bootstrap-material-design/js/material.min.js"></script> <!-- Bootstrap Material -->
<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/bootstrap-material-design/js/ripples.min.js"></script> <!-- Bootstrap Material -->

<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/js/application.js"></script>
<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/demo/demo.js"></script>
<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/demo/demo-switcher.js"></script>

    <!-- End loading page level scripts-->
<script>
    // See Docs
    window.ParsleyConfig = {
        successClass: 'has-success'
        , errorClass: 'has-error'
        , errorElem: '<span></span>'
        , errorsWrapper: '<span class="help-block"></span>'
        , errorTemplate: "<div></div>"
        , classHandler: function(el) {
            return el.$element.closest(".form-group");
        }
    };
</script>
<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/plugins/form-parsley/parsley.js"></script>  					<!-- Validate Plugin / Parsley -->
<script src="http://willcoonline.com.ng/richmond-gate1/assets/assets/demo/demo-formvalidation.js"></script>
</body>

</html>