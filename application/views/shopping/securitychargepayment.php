<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        
       <div class="page-heading">
            <h1>Security Charge Payment History</h1>

            <div class="options">
                <form action="<?php echo site_url('shopping/securitychargepayment') ?>" method="post">
                             <span>From :</span><input type="text" name="date_from" id="date_from"  value="<?php echo $date_from; ?>">
                             <span>To : </span><input type="text" name="date_to" id="date_to"  value="<?php echo $date_to; ?>">
                             <input type="submit" name="Search" value="Search">
                             
                             </form>
            </div>
        </div>
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2>Security Charg payment history</h2>

                            <div class="panel-ctrls"></div>
                        </div>
                        <div class="panel-body">
                            <table id="defaultTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Payment ID</th>
                                    <?php if ($this->aauth->is_member('Admin')):?>
                                    <th>Meter No</th>
                                    <th>Owner Name</th>
                                    <?php endif; ?>
                                    <th>Amount</th>
                                    <?php if ($this->aauth->is_member('Admin')):?>
                                    <th>Total Amount Paid</th>
                                    <?php endif; ?>                                    
                                    <th>Product</th>
                                   <?php /*?> <th>Description</th><?php */?>
                                    <th>Payment Date</th>
                                    <th>Status</th>
                                    <th>Reason</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($payments as $payment): ?>
                                <tr class="odd gradeX">
                                    <td><?=$payment->payment_id; ?></td>
                                    <?php if ($this->aauth->is_member('Admin')):?>
                                        <td><?=$payment->meter_no; ?></td>
                                        <td><?=$payment->full_name; ?></td>
                                    <?php endif; ?>
                                    <td><?=number_format($payment->amount); ?></td>
                                    <td><?=number_format($payment->amount_paid); ?></td>                                    
                                    <th><?=$payment->product_name; ?></th>
                                   <?php /*?> <td><?=$payment->narration; ?></td><?php */?>
                                    <td> <?=$payment->payment_date; ?></td>
                                    <td><?=$payment->status; ?></td>
                                    <td><?=$payment->payment_description; ?></td>
                                </tr>
                                <?php endforeach; ?>
                                 </tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

