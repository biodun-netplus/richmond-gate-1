<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        
       <div class="page-heading">
            <h1>Services Payment Informations</h1>

            <div class="options">
                <form action="<?php echo site_url('shopping/servicechargedebt') ?>" method="post">
                             <span>From :</span><input type="text" name="date_from" id="date_from"  value="<?php echo $date_from; ?>">
                             <span>To : </span><input type="text" name="date_to" id="date_to"  value="<?php echo $date_to; ?>">
                             <input type="submit" name="Search" value="Search">
                             
                             </form>
            </div>
        </div>
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2>Service charges</h2>

                            <div class="panel-ctrls"></div>
                        </div>
                        <div class="panel-body">
                            <table id="defaultTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <?php if ($this->aauth->is_member('Admin')):?>
                                    <th>Meter No</th>
                                    <th>Owner Name</th>
                                    <?php endif; ?>
                                    <th>Property Type</th>
                                    <th>Amount due (&#x20A6;)</th>
                                    <th>Total Paid</th>
                                    <th>Advance Payment</th>
                                    <th>Months</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($charges as $charge): ?>
                                <tr class="odd gradeX">
                                    <?php if ($this->aauth->is_member('Admin')):?>
                                        <td><?=$charge->meter_no; ?></td>
                                        <td><?=$charge->full_name; ?></td>
                                    <?php endif; ?>
                                    <td> <?=$charge->property_type; ?></td>
                                    <td><?=number_format($charge->amount_due); ?></td>
                                   
                                    <td><?=number_format($charge->total_paid); ?></td>
                                    <td><?= number_format($charge->advance_payment); ?></td>
                                    <td><?= number_format($charge->advance_month); ?></td>
                                    <td><a href="<?php echo site_url('shopping/servicechargedept_edit/'.$charge->meter_no); ?>">edit</a></td>

                                </tr>
                                <?php endforeach; ?>
                                 </tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

