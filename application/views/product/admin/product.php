<style>
    .list-group-item-text {
        font-weight: bold;
        font-size: 16px;
    }
</style>
<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant') || $this->aauth->is_member('Public')): ?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no; ?></span>
        <?php endif; ?>

        <div class="page-heading">
            <h1>Product</h1>

            <div class="options">

            </div>
        </div>
        <div class="container-fluid">


            <div data-widget-group="group1">

                <?php if ($this->aauth->is_member('Public')) { ?>
                    <?php if(count($outstanding) == 0):?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-controls dropdown">
                                    <button class="btn btn-icon-rounded refresh-panel"><span
                                            class="material-icons inverted">refresh</span></button>

                                </div>
                                <div class="panel-body no-padding table-responsive">
                                    <div class="p-md">
                                        <h4 class="mb-n">Power</h4>
                                    </div>
                                    <div class="grid-group">
                                        <?php foreach ($productpower as $productInfo): ?>
                                            <div class="col-md-3">
                                                <div class="grid-group-item">

                                                    <div class="row-content">
                                                        <p class="list-group-item-text"><?php echo $productInfo->product_name ?></p>
                                                        <!-- <h4 class="list-group-item-heading">
                                                            &#x20A6;<?php echo number_format($productInfo->product_price); ?></h4> -->
                                                            

                                                        <p>
                                                            <!-- TODO// Check if the person has done one time payment-->
                                                            <?php
                                                            //if service_charges date_created is less than current date


                                                            if ($serviceChargeDate && $waterPayments->totalpaid >= $dueWaterAmount && $securityPayments->totalpaid >= $dueSecurityAmount):
                                                                ?>
                                                               
                                                                <?php
                                                                // Create form and send values in 'shopping/add' function.
                                                                echo form_open('shopping/add');
                                                                echo form_hidden('bill_payment', 'true');
                                                                echo form_hidden('id', $productInfo->id);
                                                                echo form_hidden('name', $productInfo->product_name);
                                                                ?>
                                                                <input type="number" class="form-control" name="price" id="price" placeholder="Enter Amount" />
                                                                <?php
                                                                //echo form_hidden('price', $productInfo->product_price);
                                                                if($lightPayments->totalpaid >= 10000){
                                                                   // do nothing
                                                                }else{
                                                                   ?> <input type="submit"
                                                                    class="btn btn-success btn-raised pull-left"
                                                                    name="action" value="Pay"/>
                                                                <?php }
                                                                ?>
                                                                

                                                                <?php
                                                                echo form_close();
                                                            endif; ?>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endforeach; ?>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-controls dropdown">
                                    <button class="btn btn-icon-rounded refresh-panel"><span
                                            class="material-icons inverted">refresh</span></button>

                                </div>
                                <div class="panel-body no-padding table-responsive">
                                    <div class="p-md">
                                        <h4 class="mb-n">Electricity Token Purchase </h4>
                                    </div>
                                    <div class="grid-group">
                                        <?php foreach ($productTopup as $productInfo): ?>
                                            <div class="col-xs-12 col-md-3">
                                                <div class="grid-group-item">

                                                    <div class="row-content">
                                                        <p class="list-group-item-text"><?php echo $productInfo->product_name ?></p>
                                                        <h4 class="list-group-item-heading">
                                                            &#x20A6;<?php echo number_format($productInfo->product_price); ?></h4>

                                                        <p>
                                                            <?php
                                                            
                                                            echo form_open('shopping/add');
                                                            echo form_hidden('id', $productInfo->id);
                                                            echo form_hidden('name', $productInfo->product_name);
                                                            echo form_hidden('price', $productInfo->product_price);
                                                            if ($lightPayments->totalpaid >= 10000) {
                                                                ?>
                                                                <input type="submit"
                                                                       class="btn btn-success btn-raised pull-left"
                                                                       name="action" value="pay"/>
                                                            <?php } ?>
                                                            <?php
                                                            echo form_close(); ?>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endforeach; ?>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div> -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-controls dropdown">
                                    <button class="btn btn-icon-rounded refresh-panel"><span
                                            class="material-icons inverted">refresh</span></button>

                                </div>
                                <div class="panel-body no-padding table-responsive">
                                    <div class="p-md">
                                        <h4 class="mb-n">Services </h4>
                                    </div>
                                    <div class="grid-group">
                                        <?php foreach ($productservices as $productInfo): ?>
                                            <div class="col-xs-12 col-md-6">
                                                <div class="grid-group-item">

                                                    <div class="row-content">
                                                        <p class="list-group-item-text"><?php echo $productInfo->product_name ?></p>
                                                        <?php
                                                            if($servicePayments->totalpaid <= $serviceDue) { ?>
                                                                <h4 class="list-group-item-heading">
                                                                    &#x20A6;<?php echo number_format($dueServiceAmount); ?></h4>
                                                                    <?php
                                                                        // Create form and send values in 'shopping/add' function.
                                                                        echo form_open('shopping/add');
                                                                        echo form_hidden('id', $productInfo->id);
                                                                        echo form_hidden('name', $productInfo->product_name);
                                                                        echo form_hidden('price', $dueServiceAmount);
                                                                        //echo form_hidden('price', $productInfo->amount_due);
                                                                        ?>
                                                                        <p style="color:red">click on settings to see payment options, quarterly & bi-annually are available</p>
                                                                        <input type="submit"
                                                                            class="btn btn-success btn-raised pull-left"
                                                                            name="action" value="pay"/>
                                                                        <a href="http://willcoonline.com.ng/richmond-gate1/index.php/profile/paymentsettings" class="btn btn-info btn-raised pull-left">Payment Options</a>

                                                                    <?php
                                                                    echo form_close(); ?>
                                                            <?php } else { ?>
                                                                <h4 class="list-group-item-heading">
                                                                    &#x20A6;<?php echo number_format('0'); ?></h4>
                                                            <?php } ?>
                                                            

                                                  
                                                            
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endforeach; ?>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Water pump -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-controls dropdown">
                                    <button class="btn btn-icon-rounded refresh-panel"><span
                                            class="material-icons inverted">refresh</span></button>
                                </div>
                                <div class="panel-body no-padding table-responsive">
                                    <div class="p-md">
                                        <h4 class="mb-n">Water Bill</h4>
                                    </div>
                                    <div class="grid-group">
                                        <?php foreach ($productwaterpump as $productInfo): ?>
                                            <div class="col-xs-12 col-md-3">
                                                <div class="grid-group-item">
                                                    <div class="row-content">
                                                        <?php if( $waterPayments->totalpaid < $dueWaterAmount) { ?>
                                                            <p class="list-group-item-text"><?php echo $productInfo->product_name ?></p>
                                                            <h4 class="list-group-item-heading">
                                                                &#x20A6;<?php echo number_format($productInfo->product_price); ?></h4>
                                                        <?php } ?>
                                                        <?php
                                                            if ($servicePayments->totalpaid >= $dueServiceAmount && $waterPayments->totalpaid < $dueWaterAmount ): ?>
                                                            <p>
                                                            <?php
                                                                // Create form and send values in 'shopping/add' function.
                                                                echo form_open('shopping/add');
                                                                echo form_hidden('id', $productInfo->id);
                                                                echo form_hidden('name', $productInfo->product_name);
                                                                echo form_hidden('price', $productInfo->product_price);

                                                                ?>
                                                                <input type="submit"
                                                                       class="btn btn-success btn-raised pull-left"
                                                                       name="action" value="Pay"/>

                                                                <?php
                                                                echo form_close();
                                                            endif; ?>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endforeach; ?>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Security Charge -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-controls dropdown">
                                    <button class="btn btn-icon-rounded refresh-panel"><span
                                            class="material-icons inverted">refresh</span></button>
                                </div>
                                <div class="panel-body no-padding table-responsive">
                                    <div class="p-md">
                                        <h4 class="mb-n">Security Charge</h4>
                                    </div>
                                    <div class="grid-group">
                                        <?php foreach ($productsecuritycharge as $productInfo): ?>
                                            <div class="col-xs-12 col-md-3">
                                                <div class="grid-group-item">

                                                    <div class="row-content">
                                                        <?php if( $securityPayments->totalpaid < $dueSecurityAmount) { ?>
                                                        <p class="list-group-item-text"><?php echo $productInfo->product_name ?></p>
                                                        <h4 class="list-group-item-heading">
                                                            &#x20A6;<?php echo number_format($productInfo->product_price); ?></h4>
                                                        <?php } ?>
                                                        <?php
                                                        if ($servicePayments->totalpaid >= $dueServiceAmount && $securityPayments->totalpaid < $dueSecurityAmount): ?>
                                                            <p>
                                                                <?php
                                                                    // Create form and send values in 'shopping/add' function.
                                                                    echo form_open('shopping/add');
                                                                    echo form_hidden('id', $productInfo->id);
                                                                    echo form_hidden('name', $productInfo->product_name);
                                                                    echo form_hidden('price', $productInfo->product_price);

                                                                    ?>
                                                                    <input type="submit"
                                                                        class="btn btn-success btn-raised pull-left"
                                                                        name="action" value="Pay"/>

                                                                <?php
                                                            echo form_close();
                                                        endif; ?>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endforeach; ?>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-controls dropdown">
                                    <button class="btn btn-icon-rounded refresh-panel"><span
                                            class="material-icons inverted">refresh</span></button>

                                </div>
                                <div class="panel-body no-padding table-responsive">
                                    <div class="p-md">
                                        <h4 class="mb-n">Others - One Time Payment </h4>
                                    </div>
                                    <div class="grid-group">
                                        <?php foreach ($otherPayments as $productInfo):  ?>
                                            <div class="col-xs-12 col-md-3">
                                                <div class="grid-group-item">

                                                    <div class="row-content">
                                                        
                                                        <?php  if(isset($productInfo->status)) { ?>
                                                            <?php if($productInfo->status === 'Paid') { ?>
                                                                <p class="list-group-item-text"><?php echo $productInfo->product_name ?></p>
                                                                <h4 class="list-group-item-heading">
                                                                   <?php echo (isset($productInfo->status) ?  $productInfo->status : ''); ?></h4>

                                                                <p>
                                                   
                                                            <?php } ?>
                                                        <?php }else{ ?>
                                                            <p class="list-group-item-text"><?php echo $productInfo->product_name ?></p>
                                                            <h4 class="list-group-item-heading">
                                                                <p>
                                                                <?php
                                                                // Create form and send values in 'shopping/add' function.
                                                                echo form_open('shopping/add');
                                                                echo form_hidden('id', $productInfo->id);
                                                                echo form_hidden('name', $productInfo->product_name);
                                                                echo form_hidden('price', $productInfo->product_price);
                                                                ?>
                                                                <input type="submit"
                                                                    class="btn btn-success btn-raised pull-left"
                                                                    name="action" value="pay"/>
                                                        <?php } ?>
                                                        

                                                            <?php
                                                            echo form_close(); ?>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endforeach; ?>
                                        <?php foreach ($productother as $productInfo):  ?>
                                            <div class="col-xs-12 col-md-3">
                                                <div class="grid-group-item">

                                                    <div class="row-content">
                                                        
                                                
                                                        <p class="list-group-item-text"><?php echo $productInfo->product_name ?></p>
                                                        <h4 class="list-group-item-heading">
                                                            &#x20A6;<?php echo number_format($productInfo->product_price); ?></h4>
                                                        <h4 class="list-group-item-heading">
                                                            <p>
                                                            <?php
                                                            // Create form and send values in 'shopping/add' function.
                                                            echo form_open('shopping/add');
                                                            echo form_hidden('id', $productInfo->id);
                                                            echo form_hidden('name', $productInfo->product_name);
                                                            echo form_hidden('price', $productInfo->product_price);
                                                            ?>
                                                            <input type="submit"
                                                                class="btn btn-success btn-raised pull-left"
                                                                name="action" value="pay"/>
                                                   
                                                        

                                                            <?php
                                                            echo form_close(); ?>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endforeach; ?>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php else: ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-controls dropdown">
                                    <button class="btn btn-icon-rounded refresh-panel"><span
                                            class="material-icons inverted">refresh</span></button>

                                </div>
                                <div class="panel-body no-padding table-responsive">
                                    <div class="p-md">
                                        <h4 class="mb-n">Outstanding Bills </h4>
                                    </div>
                                    <div class="grid-group">
                                        <?php foreach ($outstanding as $bill): ?>
                                            <div class="col-xs-12 col-md-3">
                                                <div class="grid-group-item">

                                                    <div class="row-content">
                                                        <p class="list-group-item-text"><?php echo $bill->description ?></p>
                                                        <h4 class="list-group-item-heading">
                                                            &#x20A6;<?php echo number_format($bill->amount); ?></h4>


                                                            <?php
                                                            // Create form and send values in 'shopping/add' function.
                                                            echo form_open('shopping/add');
                                                            echo form_hidden('bill_payment', 'true');
                                                            echo form_hidden('id', $bill->id);
                                                            echo form_hidden('name', $bill->description);
                                                            echo form_hidden('price', $bill->amount);
                                                            ?>
                                                            <input type="submit"
                                                                   class="btn btn-success btn-raised pull-left"
                                                                   name="action" value="pay"/>

                                                            <?php
                                                            echo form_close(); ?>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endforeach; ?>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <?php endif; ?>
                <? } else { ?>


                <?php } ?>
            </div>


        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
                