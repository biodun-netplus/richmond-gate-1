<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        
        <div class="page-heading">
            <h1>Product</h1>

            <div class="options">

            </div>
        </div>
        <div class="container-fluid">
		<?php if(!empty($this->session->flashdata('flashMsg'))){?>
                <div class="alert alert-success">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo $this->session->flashdata('flashMsg')?></span>
                </div>
           <?php } ?>

            <div data-widget-group="group1">
            
            <?php if(isset($actionType) && $actionType=='edit'){ ?>
               <div class="col-md-12">
                                <div id="form-errors" class="row"></div>

                                <div id="customer-info" class="row">
                                
                                <form action="<?php echo base_url().'product/product_add/edit/'.$records['id'];?>" method="post" enctype="multipart/form-data">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Product Name</label>
                                            <input class="form-control" data-error="Please input Product Name" value="<?php echo $records['product_name']?>" placeholder="Enter Product Name" required="required" type="text" name="product_name">
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Product Partner Type</label>
                                            <select name="partner_type" id=""  class="form-control">
                                              <option value=""> Select Partner Type</option>
                                              <option <?php if($records['partner_type']=='Partner'){ ?> selected <?php } ?> value="Partner">Partner</option>
                                              
                                              </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Product Price</label>
                                            <input class="form-control" data-error="Please input Product Price" value="<?php echo $records['product_price']?>" placeholder="Enter Product Price"  type="number" name="product_price">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>

                                   
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Payment Occurence</label>
                                            <select class="form-control" name="payment_occurrence">
                                                <option>Select Payment Occurence</option>
                                                <option  <?php if($records['payment_type_id'] ==  1){ ?> selected <?php } ?> value="1">Annually</option>
                                                <option  <?php if($records['payment_type_id'] ==  2){ ?> selected <?php } ?> value="2">Bi-annually</option>
                                                <option  <?php if($records['payment_type_id'] ==  3){ ?> selected <?php } ?> value="3">Quarterly</option>
                                                <option  <?php if($records['payment_type_id'] ==  4){ ?> selected <?php } ?> value="4">Bi-monthly</option>
                                                <option  <?php if($records['payment_type_id'] ==  5){ ?> selected <?php } ?> value="5">Monthly</option>
                                                <option  <?php if($records['payment_type_id'] ==  7){ ?> selected <?php } ?> value="7">Others</option>
                                            </select> 
                                        </div>
                                    </div>
                        
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Property Type</label>
                                            <select name="property_type" id=""  class="form-control">
                                             <option>Select Property</option>
                                                <option <?php if($records['property_type']=='Semi Detached'){ ?> selected <?php } ?> value="Semi Detached">Semi-Detached</option>
                                            
                                                <option <?php if($records['property_type']=='Terrace'){ ?> selected <?php } ?> value="Terrace">Terrace</option>
                                                <option <?php if($records['property_type']=='Flat'){ ?> selected <?php } ?> value="Flat">Flats (3-BR)</option>
                                            
                                                <option <?php if($records['property_type']=='Maisonettes'){ ?> selected <?php } ?> value="Maisonettes">Maisonettes (4-BR)</option>
                                                <option <?php if($records['property_type']=='Quads'){ ?> selected <?php } ?> value="Quads">Quads (4- & 5-BR)</option>
                                                <option <?php if($records['property_type']=='Townhouses'){ ?> selected <?php } ?> value="Townhouses">Townhouses (4-BR)</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Product Type</label>
                                            <select name="product_type" id=""  class="form-control">
                                              <option value=""> Select Product Type</option>
                                              <option <?php if($records['product_type']=='Power'){ ?> selected <?php } ?> value="Power">Power </option>
                                              <option <?php if($records['product_type']=='Service'){ ?> selected <?php } ?> value="Service">Service</option>
                                              <option <?php if($records['product_type']=='Water Pump'){ ?> selected <?php } ?> value="Water Pump">Water Pump</option>
                                              <option <?php if($records['product_type']=='Security Charge'){ ?> selected <?php } ?> value="Security Charge">Security Charge</option>
                                               <option <?php if($records['product_type']=='Other'){ ?> selected <?php } ?> value="Other">Other</option>
                                              
                                              
                                              </select>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Account Number </label>
                                            <input class="form-control" data-error="Please input Account Number" value="<?php echo $records['account_number']?>" placeholder="Enter Account Number" type="text" name="account_number">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Bank Name</label>
                                            <input class="form-control" data-error="Please input Bank Name" value="<?php echo $records['bank_name']?>" placeholder="Enter Bank Name" type="text" name="bank_name">
                                        </div>
                                    </div>
                                    
                                   <div class="col-sm-12 center">
                                        <div class="form-group">
                                          <input type="submit" name="save" value="Save" class="btn btn-primary btn-raised pull-right">
                                        </div>
                                    </div>
                                   </form> 

                                </div>
                                




                            </div>
                            <?php }else{ ?>
                            <div class="col-md-12">
                                <div id="form-errors" class="row"></div>

                                <div id="customer-info" class="row">
                                
                                <form action="<?php echo base_url().'product/product_add/add';?>" method="post" enctype="multipart/form-data">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Product Name</label>
                                            <input class="form-control" data-error="Please input Product Name" placeholder="Enter Product Name" required="required" type="text" name="product_name">
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Product Partner Type</label>
                                            <select name="partner_type" id=""  class="form-control">
                                              <option value=""> Select Partner Type</option>
                                              <option value="Partner">Partner</option>
                                              </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Product Price</label>
                                            <input class="form-control" data-error="Please input Product Price"  placeholder="Enter Product Price"  type="number" name="product_price">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Payment Occurence</label>
                                            <select class="form-control" name="payment_occurrence">
                                                <option>Select Payment Occurence</option>
                                                <option value="1">Annually</option>
                                                <option value="2">Bi-annually</option>
                                                <option value="4">Quarterly</option>
                                                <option value="6">Bi-monthly</option>
                                                <option value="12">Monthly</option>
                                                <option value="0">Others</option>
                                            </select> 
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Property Type</label>
                                            <select name="property_type" id=""  class="form-control">
                                            <option>Select Property</option>
                                            <option value="Semi Detached">Semi Detached</option>
                                            <option value="Terrace">Terrace</option>
                                            <option value="Flat">Flats (3-BR)</option>
                                            <option value="Maisonettes">Maisonettes (4-BR)</option>
                                            <option value="Quads">Quads (4- & 5-BR)</option>
                                            <option value="Townhouses">Townhouses (4-BR)</option>
                                              </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Product Type</label>
                                            <select name="product_type" id=""  class="form-control">
                                                <option value=""> Select Product Type</option>
                                                <option value="Power">Power </option>
                                                <option value="Service">Service</option>
                                                <option value="Water Pump">Water Pump</option>
                                                <option value="Security Charge">Security Charge</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Account Number</label>
                                            <input class="form-control" data-error="Please input Account Number" placeholder="Enter Account Number" type="text" name="account_number" required="required">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Bank Name</label>
                                            <input class="form-control" data-error="Please input Bank Name" placeholder="Enter Bank Name" type="text" name="bank_name" required="required">
                                        </div>
                                    </div>
                                    
                                   <div class="col-sm-12 center">
                                        <div class="form-group">
                                          <input type="submit" name="save" value="Save" class="btn btn-primary btn-raised pull-right">
                                        </div>
                                    </div>
                                   </form> 

                                </div>
           

                            </div>
                            <?php } ?>
                            
                <div class="row">
                    
                </div>

            </div>


        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
                