<div class="main">
                <div class="breadcrumb">
                    <a href="#">Administrator</a>
                    <span class="breadcrumb-devider">/</span>
                    <a href="<?php echo current_url(); ?>">Users</a>
                </div>
                <div class="content">
                    <div class="panel">
                        <div class="content-header no-mg-top">
                            <i class="fa fa-newspaper-o"></i>
                            <div class="content-header-title">Admin Users</div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <?php $this->load->view('includes/notification'); ?>
                                    
                                <div class="content-box">
                                    <div class="content-box-header">
                                        <div class="row">
                                            <div class="col-md-6 sm-max sm-text-center">
                                                <a class="btn btn-primary sm-max" href="<?php echo site_url('admin/user/new') ?>"><i class="fa fa-plus"></i> Add New User</a>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                                    <input class="form-control" type="text" placeholder="Search">
                                                </div>

                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">ID</th>
                                                    <th>User Name</th>
                                                    <th>Email</th>
                                                    <th>Last Login</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($users as $user):?>
                                                <tr>
                                                    <th class="text-center"><?php echo $user->id; ?></th>
                                                    <td><?php echo $user->username; ?></td>
                                                    <td><?php echo $user->email; ?></td>
                                                    <td><?php echo $user->last_login; ?></td>
                                                    <td><?php echo ($user->banned == 0) ? 'Active' : 'De-activated'; ?></td>
                                                    <td><a href="<?php echo site_url('admin/user/edit/'.$user->id) ?>"><i class="fa fa-pencil"></i></a><a href="<?php echo site_url('admin/user/delete/'.$user->id) ?>" onclick="return confirm('Are you sure you want to delete user?')"> <i class="fa fa-trash"></i></a></td>
                                                </tr>
                                                <?php endforeach; ?>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="content-box-footer">
                                        <div class="row">
                                            <div class="col-md-12 sm-max">
                                                <ul class="pagination sm-mgtop-5 pull-right ">
                                                    <?php foreach ($links as $link) {
                                                        echo $link;
                                                    } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>