<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        <ol class="breadcrumb">

            <li class=""><a href="index.html">Home</a></li>
            <li class="active"><a href="index.html">Dashboard</a></li>

        </ol>
        <div class="page-heading">
            <h1>Profile Settings</h1>

            <div class="options">

            </div>
        </div>
        <div class="container-fluid">

            <?php $this->load->view('includes/notification'); ?>

            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Profile</h2>
                    </div>

                    <div class="panel-body">
                        <div class="form-group mb-md">
                            <div class="col-xs-8 col-xs-offset-2">
                                <input type="text" readonly class="form-control" name="fullname" id="FulltName"
                                       placeholder="Full Name" value="<?= $this->aauth->get_user()->full_name ?>" required>
                            </div>

                        </div>
                        
                        <div class="form-group mb-md">
                            <div class="col-xs-8 col-xs-offset-2">
                                <input type="email" readonly class="form-control" data-parsley-type="email" name="email"
                                       id="Email" placeholder="Email" value="<?= $this->aauth->get_user()->email ?>"
                                       required>
                            </div>
                        </div>
                        <br>
                        <div class="form-group mb-md">
                            <div class="col-xs-8 col-xs-offset-2">
                                <p><b>Last Login:</b> <?= $this->aauth->get_user()->last_login ?></P>
                            </div>
                        </div>

                        <?php if ($this->aauth->is_member('Merchant') or $this->aauth->is_member('Public')):?>
                        
                        <div class="form-group mb-md">
                            <div class="col-xs-8 col-xs-offset-2">
                                <input type="text" readonly class="form-control" name="mobile_no" id="meterno"
                                       placeholder="Mobile Number" value="<?= $this->aauth->get_user()->mobile_no ?>" required>
                            </div>
                        </div>
                        
                        <div class="form-group mb-md">
                            <div class="col-xs-8 col-xs-offset-2">
                                <input type="text" readonly class="form-control" name="meter_no" id="meterno"
                                       placeholder="Meter Number" value="<?= $this->aauth->get_user()->meter_no ?>" required>
                            </div>
                        </div>
                        
                        
                        <div class="form-group mb-md">
                            <div class="col-xs-8 col-xs-offset-2">
                                <input type="text" readonly class="form-control" name="cug_no" id="cug_no"
                                       placeholder="CUG Number" value="<?= $this->aauth->get_user()->cug_no ?>" >
                            </div>
                        </div>
                        
                        
                         <div class="form-group mb-md">
                            <div class="col-xs-8 col-xs-offset-2">
                                <input type="text" readonly class="form-control" name="house_address" id="house_address"
                                       placeholder="House Address" value="<?= $this->aauth->get_user()->house_address ?>" required>
                            </div>
                        </div>
                        
                        <div class="form-group mb-md">
                            <div class="col-xs-8 col-xs-offset-2">
                               <select readonly class="form-control" name="type_of_property" required>
                                    <option>Select Property</option>
                                    <option <?php if($this->aauth->get_user()->type_of_property=='Semi Detached'){?> selected <?php } ?> value="Semi Detached">Semi-Detached duplexes (4-BR)</option>
                                    <option <?php if($this->aauth->get_user()->type_of_property=='Terrace'){?> selected <?php } ?> value="Terrace">Terraced houses (4-BR)</option>
                                    <option <?php if($this->aauth->get_user()->type_of_property=='Flat'){?> selected <?php } ?> value="Flat">Flats (3-BR)</option>
                                    <option <?php if($this->aauth->get_user()->type_of_property=='Maisonettes'){?> selected <?php } ?> value="Maisonettes">Maisonettes (4-BR)</option>
                                    <option <?php if($this->aauth->get_user()->type_of_property=='Quads'){?> selected <?php } ?> value="Quads">Quads (4- & 5-BR)</option>
                                    <option <?php if($this->aauth->get_user()->type_of_property=='Townhouses'){?> selected <?php } ?> value="Townhouses">Townhouses (4-BR)</option>
                                </select>
                            </div>
                        </div>
                         
                        
                        <?php endif; ?>
                        
                        

                    </div>
                    <div class="panel-footer">
                        <div class="clearfix">

                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Change Password</h2>
                    </div>
                    <?php echo form_open('profile/setting/change-password', ['id' => "validate-form", "class" => "form-horizontal"]); ?>

                    <div class="panel-body">
                        <div class="form-group mb-md">
                            <div class="col-xs-8 col-xs-offset-2">
                                <input type="password" class="form-control" name="current_password" id="old{assword"
                                       placeholder="Old password" required>
                            </div>
                        </div>
                        <div class="form-group mb-md">
                            <div class="col-xs-8 col-xs-offset-2">
                                <input type="password" class="form-control" name="password" id="Password"
                                       placeholder="New Password" required>
                            </div>
                        </div>
                        <div class="form-group mb-md">
                            <div class="col-xs-8 col-xs-offset-2">
                                <input type="password" class="form-control" name="confirm_password" id="ConfirmPassword"
                                       placeholder="Confirm new password" required data-parsley-equalto="#Password">
                            </div>
                        </div>



                    </div>
                    <div class="panel-footer">
                        <div class="clearfix">
                            <button type="submit" class="btn btn-primary btn-raised pull-right">Change Password</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>

          
            
        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
