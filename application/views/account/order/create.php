<style>
    p.text-error {
        color: white;
        padding: 10px;
        background: #e24f63;
        margin-bottom: 10px;
        width: 100%;
    }
</style>
<div class="main">
    <div class="breadcrumb">
        <a href="<?php echo site_url('order/list') ?>">Orders</a>
        <span class="breadcrumb-devider">/</span>
        <a href="#">Create</a>
    </div>
    <div class="content">
        <div class="panel">
            <div class="content-header no-mg-top">
                <i class="fa fa-newspaper-o"></i>
                <div class="content-header-title">Create New Order</div>
            </div>

            <div class="row">
                <div class="col-md-8" style="float:none;margin:auto;">
                    <?php $this->load->view('includes/notification'); ?>

                    <div class="content-box">
                        <div class="content-box-header" style="border-bottom:0px;">
                            <div class="box-title">
                                <div class="col-md-12">
                                    <div class="form-wizard-nav">
                                        <div class="step" data-form="#customer-info">Customer Info</div>
                                        <div class="step" data-form="#order-form">Order</div>
                                        <div class="step" data-form="#delivery-form">Delivery</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <?php
                            $attributes = array( 'id' => 'form-validate');
                            echo form_open('order/create', $attributes);
                        ?>
                            <div class="col-md-12">
                                <div id="form-errors" class="row"></div>

                                <div id="customer-info" class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input class="form-control" data-error="Please input customer's First Name" placeholder="Enter First Name" required="required" type="text" name="first_name">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input class="form-control" data-error="Please input customer's Last Name" placeholder="Enter Last Name" required="required" type="text" name="last_name">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Email address</label>
                                            <input class="form-control" data-error="Customer's email address is invalid" placeholder="Enter Email Address" required="required" type="email" name="email">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Phone Number</label>
                                            <input class="form-control" data-error="Please input customer's Phone Number" placeholder="Enter Phone Number" required="required" type="text" name="phone">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button class="btn btn-info next-action  pull-right" type="button">Add Order Items</button>
                                    </div>
                                    

                                </div>
                                <div id="order-form" class="row">
                                    <div class="col-md-12" style="padding:10px 15px 15px 10px;margin-bottom:10px;">
                                        <div class="col-md-12" style="padding:0px;" id="order">
                                            <div class="row">
                                                <div class="col-sm-7">

                                                    <div class="form-group">
                                                        <label>Item</label>
                                                        <input class="form-control" data-error="Please enter order item" placeholder="Enter Item" required="required" type="text" name="order_item[]">
                                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Price</label>
                                                        <input class="form-control item-price" data-error="Please enter order price" placeholder="Price" required="required" type="text" number onchange="cal_price();" name="item_price[]" pattern="\d+(\.\d{1,2})?" data-pattern-error="Price must be numeric">
                                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <button class="btn btn-default pull-right" type="button" id="add_item"><i class="fa fa-plus"></i> </button>
                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-12" style="margin-bottom: 10px;">
                                            <b>Total : </b> <span id="total">NGN0.00</span>
                                        </div>
                                    <div class="col-sm-12">
                                        <button class="btn btn-info prev-action pull-left" type="button">Prev</button>
                                        <button class="btn btn-info next-action  pull-right" type="button">Next</button>
                                    </div>
                                </div>




                            </div>

                            <div id="delivery-form" class="row">
                                <div  id="is-delivery" class="row col-sm-12">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Is delivery required?</label>
                                        <select id="is_delivery" name="is_delivery" class="form-control" required="required">
                                                        <option></option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                
                                </div>
                                


                                <div class="content-box-footer" style="width:100%;">
                                    <div class="col-sm-12">
                                        <button class="btn btn-info prev-action pull-left" type="button">Prev</button>
                                        <button class="btn btn-success pull-right" type="submit"><i class="fa fa-check"></i> Create Order</button>
                                    </div>
                                </div>
                            </div>

                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

        $('.next-action').on('click', function(e) {
            e.preventDefault();
        });
        $('#add_item').on('click', function() {

            $('#order').append('<div class="row"><div class="col-sm-7"><div class="form-group"><input class="form-control" data-error="Please enter order item" placeholder="Enter Item" required="required" type="text" name="order_item[]"  ><div class="help-block form-text with-errors form-control-feedback"></div></div></div><div class="col-sm-3" ><div class="form-group"><input class="form-control item-price" data-error="Please enter item price" placeholder="Price" required="required" onchange="cal_price();" type="text" name="item_price[]"  pattern="\\d+(\\.\\d{1,2})?" data-pattern-error="Price must be numeric"><div class="help-block form-text with-errors form-control-feedback"></div></div></div><div class="col-sm-2" style="padding: 0px 5px;"><button class="btn btn-danger remove" type="button"><i class="fa fa-remove"></i></button></div></div>');
            $('#form-validate').validator('update')
            return false; //prevent form submission
        });

        $('#order').on('click', '.remove', function() {
            $(this).parent().parent().remove();
            $('#form-validate').validator('update');
            return false; //prevent form submission
        });

        $('#form-validate').on('validate.bs.validator', function(e){
                 var hasErrors = $('#form-validate').validator().has('.has-error').length;
                if (hasErrors) {
                    $('#form-errors').html('<p class="text-error">Some fields are empty</p>');
                }else{
                    $('#form-errors').html('');
                }
           
        });
        

        $("#is_delivery").change(function() {
            var selected = $(this).val();
            //alert(selected);
            if (selected == 1) {
                $.get("<?php echo site_url('order/form/component/delivery'); ?>", function(data, status) {

                    $('#is-delivery').append(data);
                    $('#form-validate').validator('update');
                    //alert("Data: " + data + "\nStatus: " + status);
                });
            } else {
                $('.delivery').remove();
                $('#form-validate').validator('destroy');
                $('#form-validate').validator('update');
            }

        });




    });

    //#00C86D

    function cal_price() {
        var amount = 0;

        $(".item-price").each(function() {
            if($(this).val() != ''){
                var price = parseFloat($(this).val());
                amount = parseFloat(amount + price);
                $('#total').html('NGN'+amount);
            }
            
        });
        //alert('price change'); 
    }
</script>

</script>