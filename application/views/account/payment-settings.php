<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        <ol class="breadcrumb">

            <li class=""><a href="index.html">Home</a></li>
            <li class="active"><a href="index.html">Dashboard</a></li>

        </ol>
        <div class="page-heading">
            <h1>Payment Settings</h1>

            <div class="options">

            </div>
        </div>
        <div class="container-fluid">

            <?php $this->load->view('includes/notification'); ?>

            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Payment Settings</h2>
                    </div>
                    <?php echo form_open('profile/paymentsettings/payment-occurrence', ['id' => "validate-form", "class" => "form-horizontal"]); ?>
                        <div class="panel-body">
                            <div class="form-group mb-md">
                                <div class="col-xs-8 col-xs-offset-2">
                                    <p>Payment Options</p>
                                    <select class="form-control" name="payment_occurrence">
                                  
                                        <option value="">Select Payment Option</option>
                                        <option  <?php if( $payment_info=='1'){?> selected <?php } ?> value="1">Annually</option>
                                        <option  <?php if($payment_info== '2'){?> selected <?php } ?> value="2">Bi-annually</option>
                                        <option  <?php if($payment_info== '4'){?> selected <?php } ?> value="4">Quarterly</option>
                                    </select> 
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="clearfix">
                                <button type="submit" class="btn btn-primary btn-raised pull-right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            
        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
