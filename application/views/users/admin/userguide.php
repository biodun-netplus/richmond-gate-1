<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        
        <div class="page-heading">
            <h1>User Guide</h1>

            <?php /*?><div class="options">
      <form action="<?php echo site_url('users/regular_user') ?>" method="post">
     <input type="text" name="date_search" id="date_search">
     <input type="submit" name="Search" value="Search"> 
     </form>

            </div><?php */?>
        </div>
        <div class="container-fluid">
        <div class="row"> 
        
		<?php if(!empty($this->session->flashdata('flashMsg'))){?>
                <div class="alert alert-success">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo $this->session->flashdata('flashMsg')?></span>
                </div>
           <?php } ?>
         
         <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h2>User Guide</h2>
                <div class="panel-ctrls"></div>
              </div>
              <div class="panel-body">

                <p>Click on the button below to download user guide</p>
                <a  class="btn btn-success btn-raised pull-left" href="http://willcoonline.com.ng/richmond-gate1/assets/userguide.jpg" download>Download</a>
              </div>
              <div class="panel-footer"></div>
            </div>
          </div>
        </div>
        
          

        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>     