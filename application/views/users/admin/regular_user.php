<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        
        <div class="page-heading">
            <h1>Resident</h1>

            <div class="options">
      <form action="<?php echo site_url('users/regular_user') ?>" method="post">
     <input type="text" name="date_search" id="date_search">
     <input type="submit" name="Search" value="Search"> 
     </form>

            </div>
        </div>
        <div class="container-fluid">
        <div class="row"> <a href="<?php echo site_url('users/regular_user/add'); ?>">
        <button class="btn btn-primary btn-raised pull-left" type="button">Add New Resident</button>
        </a>
        
		<?php if(!empty($this->session->flashdata('flashMsg'))){?>
                <div class="alert alert-success">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo $this->session->flashdata('flashMsg')?></span>
                </div>
           <?php } ?>
         <?php if(isset($actionType) && $actionType=='add'){?>
            <div data-widget-group="group1">
                <div class="col-md-12">
                                <div id="form-errors" class="row"></div>

                                <div id="customer-info" class="row">
                                
                                <form action="<?php echo base_url().'users/regular_user/add';?>" method="post" enctype="multipart/form-data">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Full Name</label>
                                            <input class="form-control" data-error="Please input Name" placeholder="Enter Name" required="required" type="text" name="full_name">
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                            <input class="form-control" data-error="Please input Mobile Nummber" placeholder="Enter Mobile Number" required="required" type="text" name="mobile_no">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    
                                      <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>CUG Number</label>
                                            <input class="form-control" data-error="Please input CUG Number" placeholder="Enter CUG Number"  type="text" name="cug_no">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Meter Number</label>
                                            <input class="form-control" data-error="Please input Meter Nummber" placeholder="Enter Meter Nummber" required="required" type="text" name="meter_no">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" data-error="Please input Email" placeholder="Enter Email" required="required" type="text" name="email">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>

                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>TYPE OF OWNERSHIP</label>
                                            
                                            <select class="form-control" name="type_of_ownership" required>
                                            <option>Select Ownership Type</option>
                                            <option value="landlord">Landlord</option>
                                            <option value="resident">Resident</option>
                                            </select>
                                            
                                            
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input class="form-control" data-error="Please input Password" placeholder="Enter Password" required="required" type="text" name="pass">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label> HOUSE ADDRESS</label>
                                            <input class="form-control" data-error="Please input House Address" placeholder="House Address" required="required" type="text" name="house_address">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>TYPE OF PROPERTY</label>
                                            
                                            <select class="form-control" name="type_of_property" required>
                                            <option>Select Property</option>
                                            <option value="Semi Detached">Semi Detached</option>
                                            <option value="Terrace">Terrace</option>
                                            <option value="Flat">Flats (3-BR)</option>
                                            <option value="Maisonettes">Maisonettes (4-BR)</option>
                                            <option value="Quads">Quads (4- & 5-BR)</option>
                                            <option value="Townhouses">Townhouses (4-BR)</option>
                                            </select>
                                            
                                            
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    
                                   <div class="col-sm-12 center">
                                        <div class="form-group">
                                          <input type="submit" name="save" value="Save" class="btn btn-primary btn-raised pull-right">
                                        </div>
                                    </div>
                                   </form> 

                                </div>
                            </div>


                <div class="row">
                    
                </div>

            </div>
        <?php } else if(isset($actionType) && $actionType=='edit'){?>
            <div data-widget-group="group1">
                <div class="col-md-12">
                                <div id="form-errors" class="row"></div>

                                <div id="customer-info" class="row">
                                <form action="<?php echo base_url().'users/regular_user/edit/'.$records['id']?>" method="post" enctype="multipart/form-data">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Full Name</label>
                                            <input class="form-control" data-error="Please input Name" value="<?php echo $records['full_name']?>" placeholder="Enter Name" required="required" type="text" name="full_name">
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Mobile Nummber</label>
                                            <input class="form-control" data-error="Please input Mobile Nummber" placeholder="Enter Mobile Nummber" required="required" type="text" name="mobile_no"  value="<?php echo $records['mobile_no']?>" >
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                     <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>CUG Number</label>
                                            <input class="form-control" data-error="Please input CUG Number" placeholder="Enter CUG Number"  type="text" name="cug_no"  value="<?php echo $records['cug_no']?>">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Meter Number</label>
                                            <input class="form-control" data-error="Please input Meter Nummber" placeholder="Enter Mobile Nummber" required="required" type="text" name="meter_no" value="<?php echo $records['meter_no']?>">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" data-error="Please input Email" placeholder="Enter Email" required="required" type="text" name="email"  value="<?php echo $records['email']?>" >
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>

                                     
                                     <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>TYPE OF OWNERSHIP</label>
                                            
                                            <select class="form-control" name="type_of_ownership" required>
                                            <option>Select Property</option>
                                            <option <?php if($records['type_of_ownership']=='landlord'){?> selected <?php } ?> value="landlord">Landlord</option>
                                            <option <?php if($records['type_of_ownership']=='resident'){?> selected <?php } ?> value="resident">Resident</option>
                                            </select>
                                            
                                            
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label> HOUSE ADDRESS</label>
                                            <input class="form-control" data-error="Please input HOUSE ADDRESS" placeholder="HOUSE ADDRESS" required="required" type="text" name="house_address"  value="<?php echo $records['house_address']?>">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>TYPE OF PROPERTY</label>
                                            
                                            <select class="form-control" name="type_of_property" required>
                                            <option>Select Property</option>
                                            <option <?php if($records['type_of_property']=='Semi Detached'){?> selected <?php } ?> value="Semi Detached">Semi Detached</option>
                                            <option <?php if($records['type_of_property']=='Terrace'){?> selected <?php } ?> value="Terrace">Terrace</option>
                                            <option <?php if($records['type_of_property']=='Detached'){?> selected <?php } ?> value="Detached">Detached</option>
                                            <option <?php if($records['type_of_property']=='Flat'){?> selected <?php } ?> value="Flat">Flats (3-BR)</option>
                                            <option <?php if($records['type_of_property']=='Maisonettes'){?> selected <?php } ?> value="Maisonettes">Maisonettes (4-BR)</option>
                                            <option <?php if($records['type_of_property']=='Quads'){?> selected <?php } ?> value="Quads">Quads (4- & 5-BR)</option>
                                            <option <?php if($records['type_of_property']=='Townhouses'){?> selected <?php } ?> value="Townhouses">Townhouses (4-BR)</option>
                                            </select>
                                            
                                            
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    
                                    
                                    <input type="hidden" name="id" value="<?php echo $records['id'];?>">
                                    
                                   <div class="col-sm-12 center">
                                        <div class="form-group">
                                          <input type="submit" name="save" value="Save" class="btn btn-primary btn-raised pull-right">
                                        </div>
                                    </div>
                                   </form> 
                                </div>
                            </div>


                <div class="row">
                    
                </div>

            </div>
        <?php }else{ ?>
         <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h2>Regular User list</h2>
                <div class="panel-ctrls"></div>
              </div>
              <div class="panel-body no-padding">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Meter No.</th>
                      <th>Cug No.</th>
                      <th>Category</th>
                      <th>Mobile No.</th>
                      <th>Address</th>
                      <th>Property Type</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
						$i=1;
						if(isset($records) && count($records)>0){
						foreach($records as $LoopRecord){?>
                    <tr>
                      <td> <a href="<?php echo base_url() ?>users/profile/<?php echo $LoopRecord->id ?>"> <?php echo $LoopRecord->full_name ?></a></td>
                      <td> <?php echo $LoopRecord->email ?></td>
                      <td> <?php echo $LoopRecord->meter_no ?></td>
                       <td> <?php echo $LoopRecord->cug_no ?></td>
                       <td><?php echo $LoopRecord->type_of_ownership ?></td>
                        <td> <?php echo $LoopRecord->mobile_no; ?></td>
                      <td> <?php echo $LoopRecord->house_address ?></td>
                      <td> <?php echo $LoopRecord->type_of_property ?></td>
                      <td><a href="<?php echo site_url('users/regular_user/edit/'.$LoopRecord->id); ?>">Edit</a>
                       |
                       <a href="<?php echo site_url('users/regular_user/delete/'.$LoopRecord->id); ?>">Delete</a>
                       </td>
                    </tr>
                    <?php $i++;}}?>
                  </tbody>
                </table>
              </div>
              <div class="panel-footer"></div>
            </div>
          </div>
        </div>
        
           <?php } ?>

        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
                